const MinifyPlugin = require("babel-minify-webpack-plugin");
const { injectBabelPlugin } = require('react-app-rewired');

module.exports = (config, env) => {
  config = injectBabelPlugin(['import', { libraryName: 'antd', libraryDirectory: 'es', style: 'css' }], config);
  config = injectBabelPlugin('transform-runtime', config);
  
  if (process.env.NODE_ENV === 'production') {
    var uglifyPluginIndex = -1;
    config.plugins.forEach((plugin, index) => {
      var pluginType = plugin.constructor && plugin.constructor.name;
      if (pluginType === 'UglifyJsPlugin') {
        uglifyPluginIndex = index;
      }
    })
    if (uglifyPluginIndex >= 0) {
      config.plugins.splice(uglifyPluginIndex, 1);
      return config;
      config.plugins[uglifyPluginIndex] = new MinifyPlugin();
    }
    else {
      config.plugins.push(new MinifyPlugin());
    }
  }


  console.log(JSON.stringify(config));
  // config.extraBabelPlugin = [
  //   "transform-runtime",
  //   ["import", { "libraryName": "antd", "style": true }]
  // ]


  return config

}