import feathersjs from '@feathersjs/client';
import rest from '@feathersjs/rest-client';
import auth from '@feathersjs/authentication-client';
import io from 'socket.io-client';
import socketio from '@feathersjs/socketio-client';
import Socket from 'socket.io-client/lib/socket';
// import socketApi from '../storex/api/socket'
// import userApi from '../storex/api/user'

global.io = io;

const host = '';
var socket = window.socket;
var feathers = window.feathers;

if (!socket || !feathers) {
  socket = window.socket = io.connect('/');
  feathers = socket.app = window.app = feathersjs()
    .configure(socketio(socket, {timeout: 120000}))
    .configure(auth({ storage: window.localStorage }));
  feathers.socket = socket;

  socket.on('connect', (payload) => {
    // socketApi.socketConnect({ connected: true, payload: payload })
  });

  socket.on('disconnect', () => {
    // socketApi.socketDisconnect({ connected: false })
  });
}

export default feathers;
