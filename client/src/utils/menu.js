module.exports = [
  {
    id: 1,  // Id should be unique for each menu item
    icon: 'pie-chart', // Menu item icon name 
    name: 'Dashboard', // Menu item name
    router: '/dashboard', // route to display, unless the item is a parent
  },
  {
    id: 2,
    bpid: 1, // Bread crumb id, the bread crumbs can have a different organization than the menu
    name: 'Users',
    icon: 'key',
    router: '/user', // The route in the parent item is just for access control
  },
  {
    id: 21,
    mpid: -1, // Parent menu item
    bpid: 2,
    name: 'User Detail',
    router: '/user/:id',
  },
  {
    id: 5,
    bpid: 1,
    name: 'Customers',
    icon: 'team',
    router: '/customers',
  },
  {
    id: 6,
    bpid: 1,
    name: 'Deposits',
    icon: 'login',
    router: '/deposits',
  },
  {
    id: 7,
    bpid: 1,
    name: 'Extractions',
    icon: 'logout',
    router: '/extractions',
  },
  {
    id: 8,
    bpid: 1,
    name: 'Peerpaid Settings',
    icon: 'setting',
    router: '/settings',
  },
];
