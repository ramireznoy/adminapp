const accessPoint = 'http://127.0.0.1:3033';
const APIV1 = '/api/v1';
const APIV2 = '/api/v2';

module.exports = {
  name: 'Peerpaid Admin',
  prefix: 'peerpaid_',
  footerText: 'Peerpaid © 2017',
  logo: '/logo.png',
  iconFontCSS: '/iconfont.css',
  iconFontJS: '/iconfont.js',
  openPages: ['/login', '/forgot', '/reset'],
  apiPrefix: '/api/v1',
  api: {
    userLogin: `${accessPoint}/authentication`,
    userLogout: `${accessPoint}/authentication`,
    userQuery: `${accessPoint}/users/:id`,
    dashboard: `${accessPoint}/dashboard`,
    userVerify: `${accessPoint}/reset`,
    userReset: `${accessPoint}/reset`,

    userMockQuery: `${APIV1}/users/:id`,
    userEmail: `${APIV1}/user/email`,
    userInfo: `${APIV1}/userInfo`,
    users: `${APIV1}/users`,
    v1test: `${APIV1}/test`,
    v2test: `${APIV2}/test`,
  },
};
