import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { message } from 'antd';
import List from './List';
import './index.css';

class Deposits extends React.Component {
  constructor(props) {
    super(props);
  }

  lockDeposit = (id) => {
    this.props.dispatch({
      type: 'deposits/lock',
      payload: {
        requestId: id,
      },
    });
  }

  refundDeposit = (id) => {
    this.props.dispatch({
      type: 'deposits/refund',
      payload: {
        requestId: id,
      },
    });
  }

  render() {
    const listProps = {
      dataSource: this.props.deposits.list,
      loading: this.props.loading.effects['deposits/query'],
      //pagination: this.props.deposits.pagination,
      //onChange: this.onChange,
      lockDeposit: this.lockDeposit,
      refundDeposit: this.refundDeposit
    };

    return (
      <div className="content-inner">
        <List {...listProps} />
      </div >
    );
  }
}

Deposits.propTypes = {
  deposits: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
};

// Mapping the properties to connect the component to the store
const mapStateToProps = (state) => {
  return {
    deposits: state.deposits,
    loading: state.loading,
  };
};

export default connect(mapStateToProps)(Deposits);