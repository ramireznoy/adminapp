import TotalCard from './totalCard';
import Sales from './sales';
import Currencies from './currencies';

export { TotalCard, Sales, Currencies };
