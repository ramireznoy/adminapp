import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Card } from 'antd';
import CountUp from 'react-countup';
import './totalCard.css';

class TotalCard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      color: this.props.color,
    };
    return (
      <Card className="numberCard" bordered={false} bodyStyle={{ padding: 0 }}>
        <Icon className="iconWarp" style={style} type={this.props.icon} />
        <div className="content">
          <p className="title">{this.props.title || 'No Title'}</p>
          <p className="number">
            <CountUp
              start={0}
              end={this.props.number}
              duration={2.75}
              useEasing
              useGrouping
              separator=","
              {...this.props.countUp || {}}
            />
          </p>
        </div>
      </Card>
    );
  }  
}

TotalCard.propTypes = {
  icon: PropTypes.string,
  color: PropTypes.string,
  title: PropTypes.string,
  number: PropTypes.number,
  countUp: PropTypes.object,
};

export default TotalCard;
