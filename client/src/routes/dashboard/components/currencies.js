import React from 'react';
import PropTypes from 'prop-types';
import { Table, Tag } from 'antd';
import './currencies.css';
import { color } from '../../../utils';

const status = {
  1: {
    color: color.green,
  },
  2: {
    color: color.red,
  },
  3: {
    color: color.blue,
  },
  4: {
    color: color.yellow,
  },
};

class Currencies extends React.Component {
  constructor(props) {
    super(props);

    this.columns = [
      {
        title: 'name',
        dataIndex: 'name',
        className: 'name',
      }, {
        title: 'percent',
        dataIndex: 'percent',
        className: 'percent',
        render: (text, it) => <Tag color={status[it.status].color}>{text}%</Tag>,
      },
    ];
    
  }  
  render() {
    return <Table pagination={false} showHeader={false} columns={this.columns} rowKey={(record, key) => key} dataSource={this.props.data} />;
  }
}

Currencies.propTypes = {
  data: PropTypes.array,
};

export default Currencies;
