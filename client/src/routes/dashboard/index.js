import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Row, Col, Card } from 'antd';
import { TotalCard, Currencies, Sales} from './components';
import { color } from '../../utils';
import './index.css';

class Dashboard extends React.Component {
  constructor(props){
    super(props);
    this.bodyStyle = {
      bodyStyle: {
        height: 432,
        background: '#fff',
      },
    };
  }
  
  render() {
    const totalCards = this.props.dashboard.totals.map((item, key) =>
    <Col key={key} lg={6} md={12}>
      <TotalCard {...item} />
    </Col>
    );
    return (
      <Row gutter={24}>
        {totalCards}
        <Col lg={12} md={24}>
          <Card bordered={false} {...this.bodyStyle}>
            <Sales data={this.props.dashboard.sales} />
          </Card>
        </Col>
        <Col lg={12} md={24}>
          <Card bordered={false} {...this.bodyStyle}>
            <Currencies data={this.props.dashboard.currencies} />
          </Card>
        </Col>
      </Row>
    );
  }
}

Dashboard.propTypes = {
  dashboard: PropTypes.object,
};

// Mapping the properties to connect the component to the store
const mapStateToProps = (state) => {
  return {
    dashboard: state.dashboard,
  };
};

export default connect(mapStateToProps)(Dashboard);
