import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { message } from 'antd';
import List from './List';

class Extractions extends React.Component {
  constructor(props) {
    super(props);
  }

  declineExtraction = (id) => {
    this.props.dispatch({
      type: 'extractions/decline',
      payload: {
        requestId: id,
      },
    });
  }

  approveExtraction = (id) => {
    this.props.dispatch({
      type: 'extractions/approve',
      payload: {
        requestId: id,
      },
    });
  }

  render() {
    const listProps = {
      dataSource: this.props.extractions.list,
      loading: this.props.loading.effects['extractions/query'],
      //pagination: this.props.extractions.pagination,
      //onChange: this.onChange,
      declineDeposit: this.declineExtraction,
      approveDeposit: this.approveExtraction
    };

    return (
      <div className="content-inner">
        <List {...listProps} />
      </div >
    );
  }
}

Extractions.propTypes = {
  deposits: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
};

// Mapping the properties to connect the component to the store
const mapStateToProps = (state) => {
  return {
    extractions: state.extractions,
    loading: state.loading,
  };
};

export default connect(mapStateToProps)(Extractions);