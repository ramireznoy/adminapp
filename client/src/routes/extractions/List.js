import React from 'react';
import PropTypes from 'prop-types';
import { Table, Button, Modal, Icon, Row } from 'antd';
import ExtractionDetails from './ExtractionDetails.js';
import classnames from 'classnames';

const confirm = Modal.confirm;

class List extends React.Component {
  constructor(props) {
    super(props);

    this.columns = [
      {
        title: 'First Name',
        dataIndex: 'firstName',
        render: (text, record) => record.customer.firstName,
      },
      {
        title: 'Last Name',
        dataIndex: 'lastName',
        render: (text, record) => record.customer.lastName,
      },
      {
        title: 'Email',
        dataIndex: 'emailAddress',
        render: (text, record) => record.customer.emailAddress,
      },
      {
        title: 'Amount',
        dataIndex: 'amount',
        render: (text, record) => record.transaction ? <span>{record.transaction.amount} {record.transaction.currency}</span> : '-',
      },
      {
        title: 'Identity verified',
        dataIndex: 'isVerified',
        key: 'isVerified',
        render: (text, record) => record.customer.isVerified ? <Icon type="check-circle" /> : '',
      },
      {
        title: 'Actions',
        key: 'actions',
        width: 100,
        render: (text, record) => {
          return <div>
            <Button.Group>
              <Button type="default" onClick={e => this.onDecline(record, e)}>
                <Icon type="close" />
              </Button>
              <Button type="primary" onClick={e => this.onProceed(record, e)}>
                <Icon type="check" />
              </Button>
            </Button.Group>
          </div>;
        },
      },
    ];

    this.getBodyWrapperProps = {
      //page: this.props.location.query.page,
      //current: this.props.pagination.current,
    };
  }

  getBodyWrapper = (body) => {
    return (
      body
    );
  };

  onDeclineOk = (record) => {
    this.props.declineDeposit(record._id);
  }

  onProceedOk = (record) => {
    this.props.approveDeposit(record._id);
  }

  onDecline = (record, e) => {
    var menuThis = this;
    confirm({
      okType: 'danger',
      title: 'Are you sure you want to decline the request?',
      onOk() {
        menuThis.onDeclineOk(record);
      },
    });
  }

  onProceed = (record, e) => {
    var menuThis = this;
    confirm({
      okType: 'primary',
      title: 'Are you sure you want to proceed with the request?',
      onOk() {
        menuThis.onProceedOk(record);
      },
    });
  }

  render() {
    return (
      <div>
        <Table
          {...this.props}
          className={classnames({ ['table']: true })}
          bordered
          scroll={{ x: 1250 }}
          columns={this.columns}
          simple
          size="small"
          rowKey={record => record._id}
          getBodyWrapper={this.getBodyWrapper}
          expandedRowRender={record => <ExtractionDetails record={record} />}
        />
      </div>
    );
  }
}

List.propTypes = {
  approveDeposit: PropTypes.func,
  declineDeposit: PropTypes.func,
};

export default List;
