import React from 'react';
import PropTypes from 'prop-types';
import { message, Collapse, Tabs, Row, Col, Tag } from 'antd';

const Panel = Collapse.Panel;
const TabPane = Tabs.TabPane;

class ExtractionDetails extends React.Component {
  constructor(props) {
    super(props);
  }

  tag = (alert) => {
    switch (alert) {
      case 'yes':
        return <Tag color="green">Yes</Tag>;
        break;
      case 'no':
        return <Tag color="red">No</Tag>;
        break;
      case 'SUCCESS':
        return <Tag color="green">SUCCESS</Tag>;
        break;
      case 'FAIL':
        return <Tag color="red">FAIL</Tag>;
        break;
      case 'PENDING':
        return <Tag color="orange">PENDING</Tag>;
        break;
      default:
        return <Tag>{alert}</Tag>;
    }
  }

  renderPair = (name, value) => {
    return (
      <Row>
        <Col push={4} span={12}>{name}:</Col>
        <Col pull={4} span={12}>{value}</Col>
      </Row>);
  }

  renderCustomer = (record) => {
    return (
      <TabPane tab="Customer" key="1">
        {this.renderPair('Full name', record.customer.firstName + ' ' + record.customer.lastName)}
        {this.renderPair('Is customer verified?', this.tag(record.customer.isVerified ? 'yes' : 'no'))}
        {this.renderPair('Is account verified?', this.tag(record.customer.emailVerified ? 'yes' : 'no'))}
      </TabPane>
    );
  }

  renderTransaction = (record) => {
    return (
      <TabPane tab="Transaction" key="2">
        {this.renderPair('Type', this.tag(record.transaction.type))}
        {this.renderPair('Created', record.transaction.createdAt)}
        {this.renderPair('Status', this.tag(record.transaction.status))}
      </TabPane>
    );
  }

  renderAccount = (record) => {
    return (
      <TabPane tab="Account" key="3">
        {this.renderPair('Id', record.account.accountId)}
        {this.renderPair('Amount', record.account.amount)}
        {this.renderPair('Currency', this.tag(record.account.currencyType))}
      </TabPane>
    );
  }

  render() {
    const record = this.props.record;
    return (
      <Tabs defaultActiveKey="1">
        {record.customer ? this.renderCustomer(record) : ''}
        {record.transaction ? this.renderTransaction(record) : ''}
        {record.account ? this.renderAccount(record) : ''}
      </Tabs>
    );
  }
}

ExtractionDetails.propTypes = {
  record: PropTypes.object,
};

export default (ExtractionDetails);