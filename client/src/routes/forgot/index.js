import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Link } from 'dva/router';
import { Button, Row, Form, Input, Alert, Icon } from 'antd';
import { config } from '../../utils';
import './index.css';

const FormItem = Form.Item;

class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
  }

  handleOk = () => {
    this.props.form.validateFieldsAndScroll((errors, values) => {
      if (errors) {
        return;
      }
      this.props.dispatch({ type: 'forgot/sendEmail', payload: values });
    });
  }

  render() {
    let view = (
      <Form>
        {
          this.props.forgot.showError ?
            <div>
              <Alert
                message="There was an error sending the email. Please, try again later..."
                type="error"
                closable
              />
              <br />
            </div> : ''
        }
        <FormItem hasFeedback >
          {this.props.form.getFieldDecorator('email', {
            rules: [
              {
                required: true,
                pattern: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
                message: 'Please, provide a valid email address',
              },
            ],
          })(<Input size="large" onPressEnter={this.handleOk} placeholder="Email address" />)}
        </FormItem>
        <Row>
          <Button type="primary" size="large" onClick={this.handleOk} loading={this.props.forgot.loginLoading}>
            Recover my access
      </Button>
          <div className="loginLink">
            <Link to="/login"><Icon type="rollback"></Icon> Back to Login</Link>
          </div>
        </Row>
      </Form>
    );
    if (this.props.forgot.showConfirmation) {
      view = (
        <Row>
          <div className="confirmationIcon">
            <Icon type="check-circle-o"></Icon>
          </div>
          <div className="confirmationText">
            We have sent you an email with further instructions
        </div>
          <div className="loginLink">
            <Link to="/login">
              <Button type="primary" size="large">
                <Icon type="rollback"></Icon> Back to Login
            </Button>
            </Link>
          </div>
        </Row>
      );
    }
    return (
      <div className="form">
        <div className="logo">
          <img alt={'logo'} src={config.logo} />
          <span>{config.name}</span>
        </div>
        {view}
      </div>
    );
  }
}

ForgotPassword.propTypes = {
  forgot: PropTypes.object,
};

// Mapping the properties to connect the component to the store
const mapStateToProps = (state) => {
  return {
    forgot: state.forgot,
  };
};

export default connect(mapStateToProps)(Form.create()(ForgotPassword));

