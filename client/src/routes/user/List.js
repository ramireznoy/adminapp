import React from 'react';
import PropTypes from 'prop-types';
import { Table, Modal } from 'antd';
import { Link } from 'dva/router';
import classnames from 'classnames';
import './List.css';
import AnimTableBody from '../../components/DataTable/AnimTableBody';
import { DropOption } from '../../components';

const confirm = Modal.confirm;

class List extends React.Component {
  constructor(props) {
    super(props);

    this.columns = [
      {
        title: 'Avatar',
        dataIndex: 'avatar',
        key: 'avatar',
        width: 64,
        className: 'avatar',
        render: (text) => <img alt={'avatar'} width={24} src={text} />,
      },
      {
        title: 'userName',
        dataIndex: 'userName',
        key: 'userName',
      },
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: (text, record) => <Link to={`user/${record.id}`}>{text}</Link>,
      },
      {
        title: 'Age',
        dataIndex: 'age',
        key: 'age',
      },
      {
        title: 'Phone',
        dataIndex: 'phone',
        key: 'phone',
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
      },
      {
        title: 'CreateTime',
        dataIndex: 'createTime',
        key: 'createTime',
      },
      {
        title: 'Operation',
        key: 'operation',
        width: 100,
        render: (text, record) => {
          return <DropOption onMenuClick={e => this.handleMenuClick(record, e)} menuOptions={[{ key: '1', name: 'Update' }, { key: '2', name: 'Delete' }]} />;
        },
      },
    ];

    this.getBodyWrapperProps = {
      page: this.props.location.query.page,
      current: this.props.pagination.current,
    };
  }

  getBodyWrapper = (body) => {
    return (
      this.props.isMotion ? <AnimTableBody {...this.getBodyWrapperProps} body={body} /> : body
    );
  };

  onOk = (record) => {
    this.props.onDeleteItem(record.id);
  }

  handleMenuClick = (record, e) => {
    if (e.key === '1') {
      this.props.onEditItem(record);
    } else if (e.key === '2') {
      var menuThis = this;
      confirm({
        okType: 'danger',
        title: 'Are you sure you want to delete this record?',
        onOk() {
          menuThis.onOk(record);
        },
      });
    }
  }

  render() {
    return (
      <div>
        <Table
          {...this.props}
          className={classnames({ ['table']: true, ['motion']: this.props.isMotion })}
          bordered
          scroll={{ x: 1250 }}
          columns={this.columns}
          simple
          size="small"
          rowKey={record => record.id}
          getBodyWrapper={this.getBodyWrapper}
        />
      </div>
    );
  }
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  isMotion: PropTypes.bool,
  location: PropTypes.object,
};

export default List;
