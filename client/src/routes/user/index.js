import React from 'react';
import PropTypes from 'prop-types';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import { Row, Col, Button, Popconfirm } from 'antd';
import List from './List';
import Filter from './Filter';
import Modal from './Modal';

//{ location, dispatch, user, loading }

class User extends React.Component {
  constructor(props) {
    super(props);
    const { list, pagination, currentItem, modalVisible, modalType, isMotion, selectedRowKeys } = this.props.user;
    const { pageSize } = pagination;

    this.filterProps = {
      isMotion: props.user.isMotion,
      filter: {
        ...props.location.query,
      },
      onFilterChange(value) {
        props.dispatch(routerRedux.push({
          pathname: props.location.pathname,
          query: {
            ...value,
            page: 1,
            pageSize: props.user.pagination.pageSize,
          },
        }));
      },
      onSearch(fieldsValue) {
        fieldsValue.keyword.length ? props.dispatch(routerRedux.push({
          pathname: '/user',
          query: {
            field: fieldsValue.field,
            keyword: fieldsValue.keyword,
          },
        })) : props.dispatch(routerRedux.push({
          pathname: '/user',
        }));
      },
      onAdd() {
        props.dispatch({
          type: 'user/showModal',
          payload: {
            modalType: 'create',
          },
        });
      },
      switchIsMotion() {
        props.dispatch({ type: 'user/switchIsMotion' });
      },
    };
  }

  handleDeleteItems = () => {
    this.props.dispatch({
      type: 'user/multiDelete',
      payload: {
        ids: this.props.user.selectedRowKeys,
      },
    });
  };

  onCancel = () => {
    this.props.dispatch({
      type: 'user/hideModal',
    });
  }

  onChange = (page) => {
    const { query, pathname } = this.props.location;
    this.props.dispatch(routerRedux.push({
      pathname,
      query: {
        ...query,
        page: page.current,
        pageSize: page.pageSize,
      },
    }));
  }

  onDeleteItem = (id) => {
    this.props.dispatch({
      type: 'user/delete',
      payload: id,
    });
  }

  onEditItem = (item) => {
    this.props.dispatch({
      type: 'user/showModal',
      payload: {
        modalType: 'update',
        currentItem: item,
      },
    });
  }

  render() {
    const listProps = {
      dataSource: this.props.user.list,
      loading: this.props.loading.effects['user/query'],
      pagination: this.props.user.pagination,
      location: this.props.location,
      isMotion: this.props.user.isMotion,
      onChange: this.onChange,
      onDeleteItem: this.onDeleteItem,
      onEditItem: this.onEditItem,
      rowSelection: {
        selectedRowKeys: this.props.user.selectedRowKeys,
        onChange: (keys) => {
          this.props.dispatch({
            type: 'user/updateState',
            payload: {
              selectedRowKeys: keys,
            },
          });
        },
      },
    };

    const modalProps = {
      item: this.props.user.modalType === 'create' ? {} : this.props.user.currentItem,
      visible: this.props.user.modalVisible,
      maskClosable: false,
      confirmLoading: this.props.loading.effects['user/update'],
      title: 'Customer Accounts',
      wrapClassName: 'vertical-center-modal',
      onCancel: this.onCancel,
    };

    return (
      <div className="content-inner">
        <Filter {...this.filterProps} />
        {
          this.props.user.selectedRowKeys.length > 0 &&
          <Row style={{ marginBottom: 24, textAlign: 'right', fontSize: 13 }}>
            <Col>
              {`Selected ${this.props.user.selectedRowKeys.length} items `}
              <Popconfirm title={'Are you sure delete these items?'} placement="left" onConfirm={this.handleDeleteItems}>
                <Button type="primary" size="large" style={{ marginLeft: 8 }}>Remove</Button>
              </Popconfirm>
            </Col>
          </Row>
        }
        <List {...listProps} />
        {this.props.user.modalVisible && <Modal {...modalProps} />}
      </div>
    );
  }
}

User.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
};

// Mapping the properties to connect the component to the store
const mapStateToProps = (state) => {
  return {
    user: state.user,
    loading: state.loading,
  };
};

export default connect(mapStateToProps)(User);
