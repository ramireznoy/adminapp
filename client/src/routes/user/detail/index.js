import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import './index.css';

class UserDetail extends React.Component {
  constructor(props) {
    super(props);
    this.content = [];

    for(let key in this.props.userDetail.data) {
      if ({}.hasOwnProperty.call(this.props.userDetail.data, key)) {
        this.content.push(<div key={key} className="item">
          <div>{key}</div>
          <div>{String(this.props.userDetail.data[key])}</div>
        </div>);
      }
    }
  }

  render() {
    return (
    <div className="content-inner">
      <div className="content">
        {this.content}
      </div>
    </div>
    );
  }
}

UserDetail.propTypes = {
  userDetail: PropTypes.object,
};

// Mapping the properties to connect the component to the store
const mapStateToProps = (state) => {
  return {
    userDetail: state.userDetail,
  };
};

export default connect(mapStateToProps)(UserDetail);
