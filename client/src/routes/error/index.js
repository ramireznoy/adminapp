import React from 'react';
import { Icon } from 'antd';
import './index.css';

class Error extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="content-inner">
        <div className="error">
          <Icon type="file-unknown" />
          <h1>404 Not Found</h1>
        </div>
      </div>
    );
  }

}

export default Error;
