import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Link } from 'dva/router';
import { Button, Row, Form, Input, Alert, Icon } from 'antd';
import { config } from '../../utils';
import './index.css';

const FormItem = Form.Item;

class ResetPassword extends React.Component {
  constructor(props) {
    super(props);
    const keys = Object.keys(this.props.location.query);
    this.resetToken = keys[0];    
  }

  checkPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('The passwords do not match');
    } else {
      callback();
    }
  }

  componentDidMount() {
    this.props.dispatch({ type: 'resetPassword/verify', payload: { token: this.resetToken } });
  }
  
  handleOk = () => {
    this.props.form.validateFieldsAndScroll((errors, values) => {
      if (errors) {
        return;
      }
      this.props.dispatch({ type: 'resetPassword/reset', payload: values });
    });
  }

  render() {
    let view = (
      <Form>
        <div className="loginText">
          Welcome back {this.props.reset.userFirstName}!. Please, provide your new credentials below
        </div>
        {
          this.props.reset.showError ?
            <div>
              <Alert
                message="There was an error while trying to set your credentials. Please, try again later..."
                type="error"
                closable
              />
              <br />
            </div> : ''
        }
        <FormItem hasFeedback validateStatus={this.props.reset.loginError ? 'error' : this.props.form.getFieldError(this)}>
          {this.props.form.getFieldDecorator('password', {
            rules: [
              {
                required: true,
                message: 'Password is required'
              },
            ],
          })(<Input size="large" type="password" onPressEnter={this.handleOk} onBlur={this.hideError} placeholder="New password" />)}
        </FormItem>
        <FormItem hasFeedback validateStatus={this.props.reset.loginError ? 'error' : this.props.form.getFieldError(this)}>
          {this.props.form.getFieldDecorator('password2', {
            rules: [
              {
                required: true,
                message: 'Password is required'
              },
              {
                validator: this.checkPassword,
              },
            ],
          })(<Input size="large" type="password" onPressEnter={this.handleOk} onBlur={this.hideError} placeholder="Reenter password" />)}
        </FormItem>
        <Row>
          <Button type="primary" size="large" onClick={this.handleOk} loading={this.props.reset.loginLoading}>
            Reset my password
          </Button>
          <div className="loginLink">
            <Link to="/login"><Icon type="rollback"></Icon> Back to Login</Link>
          </div>
        </Row>
      </Form>
    );
    if (this.props.reset.verifying && this.resetToken) {
      view = (
        <Row>
        <div className="verifyingIcon">
          <Icon type="loading"></Icon>
        </div>
        <div className="confirmationText">
          Please, let us verify your request, it will be quick...
        </div>
        <div className="loginLink">
          <Link to="/login">
            <Button type="primary" size="large">
              <Icon type="rollback"></Icon> Go to Login
            </Button>
          </Link>
        </div>
      </Row>
      );
    } else if (this.props.reset.showConfirmation) {
      view = (
        <Row>
          <div className="confirmationIcon">
            <Icon type="check-circle-o"></Icon>
          </div>
          <div className="confirmationText">
            Your new password has been set. You can now go back to login
          </div>
          <div className="loginLink">
            <Link to="/login">
              <Button type="primary" size="large">
                <Icon type="rollback"></Icon> Go to Login
              </Button>
            </Link>
          </div>
        </Row>
      );
    } else if (this.props.reset.saySorry || !this.resetToken) {
      let message = this.props.reset.message;
      if (!this.resetToken) {
        message = 'you are not authorized. Plase, note that the links provided are valid for just 48h, and are discarded after used.';
      }
      view = (
        <Row>
          <div className="sorryIcon">
            <Icon type="frown-o"></Icon>
          </div>
          <div className="confirmationText">
            Sorry, {message}
          </div>
          <div className="loginLink">
            <Link to="/login">
              <Button type="primary" size="large">
                <Icon type="rollback"></Icon> Go to Login
              </Button>
            </Link>
          </div>
        </Row>
      );
    }
    return (
      <div className="form">
        <div className="logo">
          <img alt={'logo'} src={config.logo} />
          <span>{config.name}</span>
        </div>
        {view}
      </div>
    );
  }
}

ResetPassword.propTypes = {
  reset: PropTypes.object,
};

// Mapping the properties to connect the component to the store
const mapStateToProps = (state) => {
  return {
    reset: state.resetPassword,
  };
};

export default connect(mapStateToProps)(Form.create()(ResetPassword));

