import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Link } from 'dva/router';
import { Button, Row, Form, Input, Alert } from 'antd';
import { config } from '../../utils';
import './index.css';

const FormItem = Form.Item;

class Login extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.dispatch({ type: 'forgot/reset' });
  }

  handleOk = () => {
    this.props.form.validateFieldsAndScroll((errors, values) => {
      if (errors) {
        return;
      }
      this.props.dispatch({ type: 'login/login', payload: values });
    });
  }

  hideError = () => {
    this.props.dispatch({ type: 'login/resetErrors' });
  }

  render() {
    return (
      <div id="crapform" className="form">
        <div className="logo">
          <img alt={'logo'} src={config.logo} />
          <span>{config.name}</span>
        </div>
        <Form>
          {
            this.props.login.badCredentials ?
              <div>
                <Alert
                  message="Invalid credentials"
                  type="error"
                  closable
                />
                <br />
              </div> : ''
          }
          <FormItem hasFeedback validateStatus={this.props.login.loginError ? 'error' : this.props.form.getFieldError(this)}>
            {this.props.form.getFieldDecorator('email', {
              rules: [
                {
                  required: true,
                  message: 'Username or Email is required'
                },
              ],
            })(<Input size="large" onPressEnter={this.handleOk} onBlur={this.hideError} placeholder="Username or Email" />)}
          </FormItem>
          <FormItem hasFeedback validateStatus={this.props.login.loginError ? 'error' : this.props.form.getFieldError(this)}>
            {this.props.form.getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Password is required'
                },
              ],
            })(<Input size="large" type="password" onPressEnter={this.handleOk} onBlur={this.hideError} placeholder="Password" />)}
          </FormItem>
          <Row>
            <Button type="primary" size="large" onClick={this.handleOk} loading={this.props.login.loginLoading}>
              Login
            </Button>
            <div className="loginLink">
              <Link to="/forgot">Forgot your password?</Link>
            </div>
          </Row>

        </Form>
      </div>
    );
  }
}

Login.propTypes = {
  login: PropTypes.object,
};

// Mapping the properties to connect the component to the store
const mapStateToProps = (state) => {
  return {
    login: state.login,
  };
};

export default connect(mapStateToProps)(Form.create()(Login));
