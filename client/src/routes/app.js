import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Icon } from 'antd';
import NProgress from 'nprogress';
import { Helmet } from 'react-helmet';
import { Layout } from '../components';
import { classnames, config, menu } from '../utils';
import '../themes/index.css';
import './app.css';
import { location } from 'react-router/lib/PropTypes';

const { prefix } = config;

const { Header, Bread, Footer, Sider, styles } = Layout;
let lastHref;
const href = window.location.href;
const { iconFontJS, iconFontCSS, logo } = config;

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  switchMenuPopover = () => {
    this.props.dispatch({ type: 'app/switchMenuPopver' });
  }

  logout = () => {
    this.props.dispatch({ type: 'app/logout' });
  }

  switchSider = () => {
    this.props.dispatch({ type: 'app/switchSider' });
  }

  changeOpenKeys = (openKeys) => {
    this.props.dispatch({ type: 'app/handleNavOpenKeys', payload: { navOpenKeys: openKeys } });
  }

  changeTheme = () => {
    this.props.dispatch({ type: 'app/switchTheme' });
  }

  changeOpenKeys = (openKeys) => {
    localStorage.setItem(`${prefix}navOpenKeys`, JSON.stringify(openKeys));
    this.props.dispatch({ type: 'app/handleNavOpenKeys', payload: { navOpenKeys: openKeys } });
  }

  getSecuredMenu = () => {
    const finalMenu = [];
    for (const index in menu) {
      let view = menu[index].router.split('/');
      if (this.props.app.user.permissions[view[1]])
        finalMenu.push(menu[index]);
    }
    return finalMenu;
  }

  isPathAllowed(path) {
    if (!path) {
      return true;
    }
    if (this.props.app.user.permissions[path]) {
      return true;
    }
    return false;
  }

  render() {
    if (lastHref !== href) {
      NProgress.start();
      if (!this.props.loading.global) {
        NProgress.done();
        lastHref = href;
      }
    }

    if (config.openPages && config.openPages.indexOf(this.props.location.pathname) > -1) {
      return (<div>{this.props.children}</div>);
    }

    /**
     * What is this!!!!???? 
     * For some reason, the user object inside app state is empty almost always in Firefox
     * Finding this took me almost an hour because in chrome it is working as expected.
     * The side effect without this, is an error while rendering the breadcrumbs component
     */
    if (Object.keys(this.props.app.user).length === 0 && this.props.app.user.constructor === Object) {
      return null;
    }

    const view = this.props.location.pathname.split('/');
    let base = view[1];
    if (view[0]) {
      base = view[0];
    }
    if (!this.isPathAllowed(base)) {
      return (
        <div className="error">
          <div className="content-inner">
            <div className={'error'}>
              <Icon type="file-unknown" />
              <h1>404 Not Found</h1>
            </div>
          </div>
        </div>
      );
    }

    const securedMenu = this.getSecuredMenu();

    const headerProps = {
      menu: securedMenu,
      user: this.props.app.user,
      siderFold: this.props.app.siderFold,
      location: this.props.location,
      isNavbar: this.props.app.isNavbar,
      menuPopoverVisible: this.props.app.menuPopoverVisible,
      navOpenKeys: this.props.app.navOpenKeys,
      switchMenuPopover: this.switchMenuPopover,
      logout: this.logout,
      switchSider: this.switchSider,
      changeOpenKeys: this.changeOpenKeys,
    };

    const siderProps = {
      menu: securedMenu,
      user: this.props.app.user,
      siderFold: this.props.app.siderFold,
      darkTheme: this.props.app.darkTheme,
      location: this.props.location,
      navOpenKeys: this.props.app.navOpenKeys,
      changeTheme: this.changeTheme,
      changeOpenKeys: this.changeOpenKeys,
    };

    const breadProps = {
      menu: securedMenu,
    };

    return (
      <div>
        <Helmet>
          <title>Peerpaid Admin</title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <link rel="icon" href={logo} type="image/x-icon" />
          {iconFontJS && <script src={iconFontJS} />}
          {iconFontCSS && <link rel="stylesheet" href={iconFontCSS} />}
        </Helmet>
        <div className={classnames('layout', { ['fold']: headerProps.isNavbar ? false : headerProps.siderFold }, { ['withnavbar']: headerProps.isNavbar })}>
          {!headerProps.isNavbar ? <aside className={classnames('sider', { ['light']: !siderProps.darkTheme })}>
            <Sider {...siderProps} />
          </aside> : ''}
          <div className={'main'}>
            <Header {...headerProps} />
            <Bread {...breadProps} location={this.props.location} />
            <div className={'container'}>
              <div className={'content'}>
                {this.props.children}
              </div>
            </div>
            <Footer />
          </div>
        </div>
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.element.isRequired,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  app: PropTypes.object,
  loading: PropTypes.object,
};

// Mapping the properties to connect the component to the store
const mapStateToProps = (state) => {
  return {
    app: state.app,
    loading: state.loading,
  };
};

export default connect(mapStateToProps)(App);
