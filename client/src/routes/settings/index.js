import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Row } from 'antd';
import ServerSettings from './ServerSettings';
import './index.css';

class Settings extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    let servers = [];
    let list = [];
    for (let [key, value] of Object.entries(this.props.settings)) {
      list.push(
        <ServerSettings key={key} params={value} />
      );
    }

    for (let i = 0; i < list.length; i += 2) {
      servers.push(
        <Row key={i}>
          {list[i]}
          {list[i + 1]}
        </Row>
      );
    }

    return (
      <div className="content-inner">
        {servers}
      </div>
    );
  }
}

Settings.propTypes = {
  settings: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
};

// Mapping the properties to connect the component to the store
const mapStateToProps = (state) => {
  return {
    settings: state.settings,
    loading: state.loading,
  };
};

export default connect(mapStateToProps)(Settings);