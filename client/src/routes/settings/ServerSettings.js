import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Button, Card, Col, Icon, Form, Input, Switch, InputNumber } from 'antd';
const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    sm: { span: 24 },
    md: { span: 8 },
  },
  wrapperCol: {
    sm: { span: 24 },
    md: { span: 16 },
  },
};

class ServerSettings extends React.Component {
  constructor(props) {
    super(props);
  }

  updateSettings = (e, serverName) => {
    this.props.form.validateFieldsAndScroll((errors, values) => {
      if (errors) {
        return;
      }
      this.props.dispatch({ type: 'settings/updateServer', payload: { values: values, server: serverName } });
    });
  }

  render() {
    let options = [];
    for (let [key, value] of Object.entries(this.props.params.settings)) {
      let name = value.name;
      let input = null;
      if (value.enabled === true) {
        switch (value.type) {
          case 'text': {
            input =
              <FormItem
                {...formItemLayout}
                label={value.title}
                help={value.description}
                key={key}
              >
                {this.props.form.getFieldDecorator(value.name, {
                  initialValue: value.value
                })(<Input placeholder={value.title} />)}
              </FormItem>;
            break;
          }
          case 'number': {
            input =
              <FormItem
                {...formItemLayout}
                label={value.title}
                help={value.description}
                key={key}
              >
                {this.props.form.getFieldDecorator(value.name, {
                  initialValue: value.value
                })(<InputNumber min={parseInt(value.minimumValue)} placeholder={value.title} />)}
              </FormItem>;
            break;
          }
          case 'boolean': {
            input =
              <FormItem
                {...formItemLayout}
                label={value.title}
                help={value.description}
                key={key}
              >
                {this.props.form.getFieldDecorator(value.name, {
                  valuePropName: 'checked',
                  initialValue: value.value
                })(<Switch checkedChildren={<Icon type="check" />} unCheckedChildren={<Icon type="cross" />} />)}
              </FormItem >;
            break;
          }
          case 'select': {
            // TODO: include select options in the settings, the best approach will be to query the model for avilable options
            // but for faster setup we could use hardcoded properties. Anyways, we can use text instead od select until
            // a very specific requirement is set
            break;
          }
        }
      }
      options.push(
        input
      );
    }

    let actions = 'No available options on this server';
    if (options.length > 0) {
      actions = <Button style={{ marginTop: '20px' }} type="primary" onClick={e => this.updateSettings(e, this.props.params.serverName)} >Update</Button>;
      if (this.props.params.updating === true) {
        actions = <Button icon="loading" style={{ marginTop: '20px' }} type="primary" onClick={e => this.updateSettings(e, this.props.params.serverName)} >Update</Button>;
      }
    }

    return (
      <Col span={12}>
        <Card
          title={this.props.params.serverName + ' server'}
          extra={<Icon type={this.props.params.icon} style={{ fontSize: '16px' }} />}
          style={{ margin: '10px 10px' }}
        >
          <Form layout="vertical">
            {options}
          </Form>
          <div style={{ textAlign: 'center' }}>
            {actions}
          </div>
        </Card>
      </Col>
    );
  }
}

ServerSettings.propTypes = {
  params: PropTypes.object,
};

// Mapping the properties to connect the component to the store
const mapStateToProps = (state) => {
  return {
    settings: state.settings,
  };
};

export default connect(mapStateToProps)(Form.create()(ServerSettings));
