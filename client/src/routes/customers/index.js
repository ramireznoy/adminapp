import React from 'react';
import PropTypes from 'prop-types';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import { Row, Col, Button, Popconfirm, Modal } from 'antd';
import List from './List';
import Filter from './Filter';
import AccountsModal from './AccountsModal';
import CustomerModal from './CustomerModal';
import DocumentsModal from './DocumentsModal';

//{ location, dispatch, user, loading }

class Customers extends React.Component {
  constructor(props) {
    super(props);
    const { list, pagination, currentItem, modalVisible, modalType, selectedRowKeys } = this.props.customers;
    const { pageSize } = pagination;

    this.filterProps = {
      filter: {
        ...props.location.query,
      },
      onFilterChange(value) {
        props.dispatch(routerRedux.push({
          pathname: props.location.pathname,
          query: {
            ...value,
            page: 1,
            pageSize: props.customers.pagination.pageSize,
          },
        }));
      },
      onSearch(fieldsValue) {
        fieldsValue.keyword.length ? props.dispatch(routerRedux.push({
          pathname: '/customers',
          query: {
            field: fieldsValue.field,
            keyword: fieldsValue.keyword,
          },
        })) : props.dispatch(routerRedux.push({
          pathname: '/customers',
        }));
      },
    };
  }

  onCancel = () => {
    this.props.dispatch({
      type: 'customers/hideAccounts',
    });
  }

  onCustomerCancel = () => {
    this.props.dispatch({
      type: 'customers/hideCustomer',
    });
  }
  
  onDocumentsCancel = () => {
    this.props.dispatch({
      type: 'customers/hideDocuments',
    });
  }

  onChange = (page) => {
    const { query, pathname } = this.props.location;
    this.props.dispatch({
      type: 'customers/paginationEvent',
      payload: {
        current: page.current,
        limit: page.pageSize,
      },
    });
    this.props.dispatch(routerRedux.push({
      pathname,
      query: {
        ...query,
        page: page.current,
        pageSize: page.pageSize,
      },
    }));
  }

  disableCustomer = (id, disabled) => {
    this.props.dispatch({
      type: 'customers/disable',
      customerId: id,
      disabledState: !disabled,
    });
  }

  openAccounts = (item) => {
    this.props.dispatch({
      type: 'customers/getAccounts',
      payload: {
        customer: item,
      },
    });
  }

  openDocuments = (item) => {
    this.props.dispatch({
      type: 'customers/getDocuments',
      payload: {
        customer: item,
      },
    });
  }

  openCustomer = (item) => {
    this.props.dispatch({
      type: 'customers/showCustomer',
      payload: {
        customer: item,
      },
    });
  }

  accountsAlertClose = () => {
    this.props.dispatch({
      type: 'customers/hideAccountsAlert',
    });
  }

  render() {
    const listProps = {
      dataSource: this.props.customers.list,
      loading: this.props.loading.effects['customers/query'],
      pagination: this.props.customers.pagination,
      location: this.props.location,
      onChange: this.onChange,
      openAccounts: this.openAccounts,
      openCustomer: this.openCustomer,
      openDocuments: this.openDocuments,
      disableCustomer: this.disableCustomer
    };

    const accountsModalProps = {
      visible: this.props.customers.accountsModalVisible,
      maskClosable: false,
      title: 'Customer Accounts',
      wrapClassName: 'vertical-top-modal',
      onCancel: this.onCancel,
    };

    const customerModalProps = {
      visible: this.props.customers.customerModalVisible,
      maskClosable: false,
      title: 'Edit Customer',
      wrapClassName: 'vertical-top-modal',
      onCancel: this.onCustomerCancel,
    };
    
    const documentsModalProps = {
      visible: this.props.customers.documentsModalVisible,
      maskClosable: false,
      title: 'Customer Documents',
      wrapClassName: 'vertical-top-modal',
      onCancel: this.onDocumentsCancel,
    };

    return (
      <div className="content-inner">
        <Filter {...this.filterProps} />
        <List {...listProps} />
        {this.props.customers.accountsModalVisible && <AccountsModal {...accountsModalProps} />}
        {this.props.customers.customerModalVisible && <CustomerModal {...customerModalProps} />}
        {this.props.customers.documentsModalVisible && <DocumentsModal {...documentsModalProps} />}
        <Modal
          title="No Currency Accounts"
          visible={this.props.customers.hasNoAccountsAlert}
          onCancel={this.accountsAlertClose}
          footer={[
            <Button key="back" type="primary" onClick={this.accountsAlertClose}>Ok</Button>,
          ]}>
          <p>The customer has no currency accounts created yet.</p>
        </Modal>
      </div >
    );
  }
}

Customers.propTypes = {
  customer: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
};

// Mapping the properties to connect the component to the store
const mapStateToProps = (state) => {
  return {
    customers: state.customers,
    loading: state.loading,
  };
};

export default connect(mapStateToProps)(Customers);
