import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Button, Modal, Form, Input, Radio, Select, Checkbox } from 'antd';
const Option = Select.Option;
const FormItem = Form.Item;
const CheckboxGroup = Checkbox.Group;

// Temporary solutions until roles are resolved.
// IMPORTANT! Need to be updated each time a new view is added
const permissionList = {
  'admin': 'Is Administrator',
  'userview': 'Frontend Access',
  'dashboard': 'Dashboard',
  'user': 'User List',
  'documents': 'Documents',
  'customers': 'Customers',
  'deposits': 'Deposits',
  'extractions': 'Extractions',
  'settings': 'Settings'
};

class CustomerModal extends React.Component {
  constructor(props) {
    super(props);
  }

  onOk = () => {
    this.props.form.validateFieldsAndScroll((errors, values) => {
      if (errors) {
        return;
      }
      let newPermissions = {};
      values.permissions.forEach(element => {
        newPermissions[element] = true;
      });
      values.permissions = newPermissions;
      this.props.dispatch({ type: 'customers/updateCustomer', payload: { values: values, customer: this.props.customers.currentCustomer._id } });
    });
  }

  getPermissionsArray() {
    let result = {
      options: [],
      values: []
    };
    if (this.props.customers.currentCustomer.permissions) {
      Object.keys(permissionList).forEach(permission => {
        result.options.push({
          'label': permissionList[permission],
          'value': permission
        });
        Object.keys(this.props.customers.currentCustomer.permissions).forEach(customerPermission => {
          if (customerPermission === permission) {
            if (this.props.customers.currentCustomer.permissions[permission] === true) {
              result.values.push(permission);
            }
          }
        });
      });
    }
    return result;
  }

  render() {
    const modalOpts = {
      ...this.props,
    };
    return (
      <Modal {...modalOpts}
        footer={[
          <Button key="back" type="default" onClick={this.props.onCancel}>Return</Button>,
          <Button key="ok" type="primary" onClick={this.onOk}>Update</Button>,
        ]}
      >
        <Form layout="vertical">
          <FormItem label="User Name">
            {this.props.form.getFieldDecorator('username', {
              rules: [
                {
                  required: true,
                  message: 'The user name is mandatory',
                }
              ],
              initialValue: this.props.customers.currentCustomer.username
            })(<Input />)}
          </FormItem>

          <FormItem label="First Name">
            {this.props.form.getFieldDecorator('firstName', {
              rules: [
                {
                  required: true,
                  message: 'Please provide a First Name',
                }
              ],
              initialValue: this.props.customers.currentCustomer.firstName
            })(<Input />)}
          </FormItem>

          <FormItem label="Middle Name">
            {this.props.form.getFieldDecorator('middleName', {
              rules: [
                {
                  required: false,
                }
              ],
              initialValue: this.props.customers.currentCustomer.middleName
            })(<Input />)}
          </FormItem>

          <FormItem label="Last Name">
            {this.props.form.getFieldDecorator('lastName', {
              rules: [
                {
                  required: true,
                  message: 'Please provide a Last Name',
                }
              ],
              initialValue: this.props.customers.currentCustomer.lastName
            })(<Input />)}
          </FormItem>

          <FormItem label="Email">
            {this.props.form.getFieldDecorator('emailAddress', {
              rules: [
                {
                  required: true,
                  message: 'Please provide a valid email address',
                }
              ],
              initialValue: this.props.customers.currentCustomer.emailAddress
            })(<Input />)}
          </FormItem>

          <FormItem label="Phone">
            {this.props.form.getFieldDecorator('phone', {
              rules: [
                {
                  required: false,
                  pattern: /^[\+]?\d/,
                  message: 'Please provide a valid phone number'
                }
              ],
              initialValue: this.props.customers.currentCustomer.phone ? this.props.customers.currentCustomer.phone.number : ''
            })(<Input />)}
          </FormItem>

          <FormItem label="Privilege Level">
            {this.props.form.getFieldDecorator('customerPrivilegeLevel', {
              rules: [
                {
                  required: false,
                }
              ],
              initialValue: this.props.customers.currentCustomer.customerPrivilegeLevel
            })(<Select>
              <Option value="NONE">None</Option>
              <Option value="FULL_AVAILABILITY">Full Availability</Option>
              <Option value="FREE_TRANSACTIONS">Free Transactions</Option>
              <Option value="FREE_PASS">Free Pass</Option>
            </Select>)}
          </FormItem>

          <FormItem label="Permissions">
            {this.props.form.getFieldDecorator('permissions', {
              rules: [
                {
                  required: false,
                }
              ],
              initialValue: this.getPermissionsArray().values
            })(<CheckboxGroup options={this.getPermissionsArray().options} />)}
          </FormItem>
        </Form>

      </Modal >

    );
  }
}

CustomerModal.propTypes = {};

// Mapping the properties to connect the component to the store
const mapStateToProps = (state) => {
  return {
    customers: state.customers,
  };
};

export default connect(mapStateToProps)(Form.create()(CustomerModal));
