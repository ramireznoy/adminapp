import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Button, Modal, Col, Card, Row, Icon, Spin, Tag, message, Collapse } from 'antd';
import DocumentView from '../../components/DocumentView';
const Panel = Collapse.Panel;

const centered = {
  textAlign: 'center',
};

class DocumentsModal extends React.Component {
  constructor(props) {
    super(props);
  }

  onList = () => {
    this.props.dispatch({ type: 'customers/switchToDocumentsList' });
  }

  render() {
    const modalOpts = {
      ...this.props,
    };
    let content = [];
    let documents = [];
    let groups = [];
    if (this.props.customers.documentsModalMode === 'list') {
      for (let [key, value] of Object.entries(this.props.customers.documentList)) {
        documents = [];
        for (var i = 0; i < value.length; i++) {
          documents.push(
            <DocumentView key={i} document={value[i]} />
          );
        }
        groups.push(
          <Panel header={key.toUpperCase()} key={key}>
            {documents}
          </Panel>
        );
      }
      if (groups.length === 0) {
        content.push(
          <div key="no-data">
            <p style={centered}>The customer has no identity documents yet</p>
            <p style={centered}>
              <Button type="default">
                <Icon type="retweet" />Request
              </Button>
            </p>
          </div>
        );
      } else {
        // TODO: Remember the opened tab to display the one containing to the last openened document
        content.push(
          <Collapse key="documents" defaultActiveKey={[this.props.customers.activeDocumentGroup]}>
            {groups}
          </Collapse>
        );
      }
    } else if (this.props.customers.documentsModalMode === 'single') {
      content.push(
        <DocumentView document={this.props.customers.currentDocument} />
      );
    }
    return (
      <Modal {...modalOpts}
        width="70%"
        footer={[
          this.props.customers.documentsModalMode === 'single' ? <Button key="list" type="default" onClick={this.onList}><Icon type="arrow-left" /> Back to List</Button> : '',
          <Button key="back" type="primary" onClick={this.props.onCancel}>Ok, done</Button>
        ]}
      >
        <Row type="flex" justify="center">
          <Col span={24}>
            <Spin tip={this.props.customers.spinText} spinning={this.props.customers.loadingDocuments}>
              {content}
            </Spin>
          </Col>
        </Row>
      </Modal>
    );
  }
}

DocumentsModal.propTypes = {};

// Mapping the properties to connect the component to the store
const mapStateToProps = (state) => {
  return {
    customers: state.customers,
  };
};

export default connect(mapStateToProps)(DocumentsModal);
