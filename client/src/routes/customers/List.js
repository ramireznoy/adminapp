import React from 'react';
import PropTypes from 'prop-types';
import { Table, Modal, Icon } from 'antd';
import { Link } from 'dva/router';
import classnames from 'classnames';
import './List.css';
import { DropOption } from '../../components';
import Moment from 'moment';

const confirm = Modal.confirm;

class List extends React.Component {
  constructor(props) {
    super(props);

    this.getBodyWrapperProps = {
      page: this.props.location.query.page,
      current: this.props.pagination.current,
    };
  }

  getBodyWrapper = (body) => {
    return (
      body
    );
  };

  onOk = (record) => {
    this.props.disableCustomer(record._id, record.disabled);
  }

  handleMenuClick = (record, e) => {
    switch (e.key) {
      case 'identity':
        this.props.openDocuments(record);
        break;
      case 'accounts':
        this.props.openAccounts(record);
        break;
      case 'disable':
        let action = 'disable';
        if (record.disabled === true) {
          action = 'enable';
        }
        var menuThis = this;
        confirm({
          okType: 'danger',
          title: 'Are you sure you want to ' + action + ' the customer?',
          onOk() {
            menuThis.onOk(record);
          },
        });
        break;
      case 'edit':
        this.props.openCustomer(record);
        break;
    }
  }

  getDisabledCustomerOption = (disabled) => {
    return disabled === true ? 'Enable Customer' : 'Disable Customer';
  }

  getDisabledCustomerIcon = (disabled) => {
    return disabled === true ? 'check' : 'close';
  }

  renderPrivilege(level) {
    switch (level) {
      case 'FREE_TRANSACTIONS':
        return 'Free Transactions';
      case 'FULL_AVAILABILITY':
        return 'Full Availability';
      case 'FREE_PASS':
        return 'Free Pass';
      default:
        return '';
    }
  }

  renderDate(date) {
    return Moment(new Date(date)).fromNow();
  }

  render() {
    let columns = [
      {
        title: 'userName',
        dataIndex: 'username',
        key: 'username',
        render: (text, record) => record.disabled ? <span><Icon type="lock" style={{ color: '#820014' }} /> {text}</span> : text,
      },
      {
        title: 'First Name',
        dataIndex: 'firstName',
        key: 'firstName',
      },
      {
        title: 'Last Name',
        dataIndex: 'lastName',
        key: 'lastName',
      },
      {
        title: 'Email',
        dataIndex: 'emailAddress',
        key: 'emailAddress',
      },
      {
        title: 'Registered',
        dataIndex: 'createdAt',
        key: 'createdAt',
        render: (text, record) => this.renderDate(record.createdAt),
      },
      {
        title: 'Privilege Level',
        dataIndex: 'customerPrivilegeLevel',
        key: 'customerPrivilegeLevel',
        render: (text, record) => this.renderPrivilege(record.customerPrivilegeLevel),
      },
      {
        title: 'Account verified',
        dataIndex: 'isVerified',
        key: 'isVerified',
        render: (text, record) => record.emailVerified ? <Icon type="check-circle" /> : '',
      },
      {
        title: 'Identity verified',
        dataIndex: 'identityVerified',
        key: 'identityVerified',
        render: (text, record) => record.identityVerified ? <Icon type="check-circle" /> : '',
      },
      {
        title: 'Actions',
        key: 'actions',
        width: 100,
        render: (text, record) => {
          let option = record.disabled ? 'Enable Customer' : 'Disable Customer';
          let icon = record.disabled ? 'check' : 'close';
          return <DropOption onMenuClick={e => this.handleMenuClick(record, e)}
            menuOptions={[
              { key: 'identity', name: 'Identity Verification', icon: 'idcard' },
              { key: 'accounts', name: 'Customer Accounts', icon: 'line-chart' },
              { key: 'contact', name: 'Contact Customer', icon: 'message' },
              { key: 'edit', name: 'Edit Customer', icon: 'edit' },
              { key: 'disable', name: option, icon: icon },
            ]} />;
        },
      },
    ];

    return (
      <div>
        <Table
          {...this.props}
          className={classnames({ ['table']: true })}
          bordered
          scroll={{ x: 1250 }}
          columns={columns}
          simple
          size="small"
          rowKey={record => record._id}
          getBodyWrapper={this.getBodyWrapper}
        />
      </div>
    );
  }
}

List.propTypes = {
  disableCustomer: PropTypes.func,
  openAccounts: PropTypes.func,
  openCustomer: PropTypes.func,
  location: PropTypes.object,
};

export default List;
