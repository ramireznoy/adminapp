import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import { connect } from 'dva';
import { message } from 'antd';
import AccountTransactions from './AccountTransactions.js';

class AccountsTable extends React.Component {
  constructor(props) {
    super(props);
    this.columns = [{
      title: 'Currency',
      dataIndex: 'currencyType',
      key: 'currencyType',
    }, {
      title: 'Amount',
      dataIndex: 'amount',
      key: 'amount',
    }, {
      title: 'Pending funds',
      dataIndex: 'tentativeAmount',
      key: 'tentativeAmount',
    }, {
      title: 'Transactions',
      dataIndex: 'transactionCount',
      render: (text, record) => record.txids.length,
    }, {
      title: 'Since',
      dataIndex: 'createdAt',
      key: 'createdAt',
      render: (text, record) => record.createdAt.split('T')[0],
    }];
  }


  onExpand = (expanded, record) => {
    if (expanded) {
      this.props.dispatch({ type: 'customers/getTransactions', account: record });
    }
  }

  render() {
    return (
      <Table
        loading={this.props.loading.effects['customers/getTransactions']}
        dataSource={this.props.customers.accountList}
        columns={this.columns}
        pagination={false}
        onExpand={this.onExpand}
        rowKey={record => record._id}
        expandedRowRender={record => <AccountTransactions dataSource={record.transactions} />}
      />
    );
  }
}

AccountsTable.propTypes = {
  item: PropTypes.object,
};

// Mapping the properties to connect the component to the store
const mapStateToProps = (state) => {
  return {
    customers: state.customers,
    loading: state.loading,
  };
};

export default connect(mapStateToProps)(AccountsTable);