import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'antd';
import AccountsTable from './AccountsTable.js';

class AccountsModal extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const modalOpts = {
      ...this.props,
    };
    return (
      <Modal {...modalOpts}
        width="70%"
        footer={[
          <Button key="back" type="primary" onClick={this.props.onCancel}>Return</Button>,
        ]}
      >
        <AccountsTable />
      </Modal>
    );
  }
}

AccountsModal.propTypes = {};

export default (AccountsModal);
