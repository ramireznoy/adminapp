import React from 'react';
import PropTypes from 'prop-types';
import { Table, Badge, message } from 'antd';
import { connect } from 'dva';
import { DropOption } from '../../components';

class AccountTransactions extends React.Component {
  constructor(props) {
    super(props);

    this.statusMap = {
      SUCCESS: 'success',
      PENDING: 'warning',
      FAIL: 'error',
    };

    this.columns = [{
      title: 'Type',
      dataIndex: 'type',
      key: 'type',
    }, {
      title: 'Amount',
      dataIndex: 'amount',
      key: 'amount',
      render: (text, record) => record.amount + ' ' + record.currency
    }, {
      title: 'Status',
      dataIndex: 'status',
      render: (text, record) => <span><Badge status={this.getBadgeStatus(record.status)} />{record.status}</span>
    }, {
      title: 'Created',
      dataIndex: 'createdAt',
      key: 'createdAt',
    }, {
      title: 'Actions',
      key: 'actions',
      width: 100,
      render: (text, record) => {
        if (record.type === 'FUNDS_DEPOSIT' && record.status === 'SUCCESS') {
          return <DropOption onMenuClick={e => this.handleMenuClick(record, e)}
            menuOptions={[
              { key: 'refund', name: 'Refund', icon: 'rollback' },
              { key: 'block', name: 'Lockdown', icon: 'lock' }
            ]} />;
        } else {
          return <DropOption onMenuClick={e => this.handleMenuClick(record, e)}
            menuOptions={[
              { key: 'block', name: 'Lockdown', icon: 'lock' }
            ]} />;
        }
      },
    }];
  }

  getBadgeStatus = (status) => {
    return this.statusMap[status];
  }

  handleMenuClick = (record, e) => {
    if (record.details && record.details.paymentTransactionId) {
      message.info('Event ' + e.key + ' Payment ID: ' + record.details.paymentTransactionId);
    } else {
      message.info('Event ' + e.key + ' No Payment ID');
    }
  }

  render() {
    return (
      <Table
        dataSource={this.props.dataSource}
        columns={this.columns}
        rowKey={record => record._id}
      />
    );
  }
}

AccountTransactions.propTypes = {
  dataSource: PropTypes.array,
};

export default (AccountTransactions);