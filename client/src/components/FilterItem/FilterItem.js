import React from 'react';
import PropTypes from 'prop-types';
import './FilterItem.css';

class FilterItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="filterItem">
        {this.props.label.length > 0 ?
          <div className="labelWrap">
            {this.props.label}
          </div>
          : ''}
        <div className="item">
          {this.props.children}
        </div>
      </div>
    );
  }
}

FilterItem.propTypes = {
  label: PropTypes.string,
  children: PropTypes.element.isRequired,
};

export default FilterItem;
