import React from 'react';
import PropTypes from 'prop-types';
import { Breadcrumb, Icon } from 'antd';
import pathToRegexp from 'path-to-regexp';
import { Link } from 'dva/router';
import './Bread.css';
import { queryArray } from '../../utils';

class Bread extends React.Component {
  constructor(props) {
    super(props);    
  }

  render() {
    // Match current route
    const pathArray = [];
    let current;
    for (let index in this.props.menu) {
      if (this.props.menu[index].router && pathToRegexp(this.props.menu[index].router).exec(window.location.pathname)) {
        current = this.props.menu[index];
        break;
      }
    }
  
    const getPathArray = (item) => {
      pathArray.unshift(item);
      if (item.bpid) {
        getPathArray(queryArray(this.props.menu, item.bpid, 'id'));
      }
    };
  
    if (!current) {
      pathArray.push(this.props.menu[0]);
      pathArray.push({
        id: 404,
        name: 'Not Found',
      });
    } else {
      getPathArray(current);
    }
    // Find the parent recursively
    const breads = pathArray.map((item, key) => {
      if (!item) return null;
      const content = (
        <span>{item.icon
          ? <Icon type={item.icon} style={{ marginRight: 4 }} />
          : ''}{item.name}</span>
      );
      return (
        <Breadcrumb.Item key={key}>
          {((pathArray.length - 1) !== key)
            ? <Link to={item.router}>
              {content}
            </Link>
            : content}
        </Breadcrumb.Item>
      );
    });
    return (
      <div className="bread">
        <Breadcrumb>
          {breads}
        </Breadcrumb>
      </div>
    );
  }
}

Bread.propTypes = {
  menu: PropTypes.array,
};

export default Bread;
