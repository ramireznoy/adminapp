import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Switch } from 'antd';
import './Layout.css';
import { config } from '../../utils';
import Menus from './Menu';

class Sider extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <div className="logo">
          <img alt={'logo'} src={config.logo} />
          {this.props.siderFold ? '' : <span>{config.name}</span>}
        </div>
        <Menus {...this.props} />
      </div>
    );
  }
}

Sider.propTypes = {
  menu: PropTypes.array,
  siderFold: PropTypes.bool,
  darkTheme: PropTypes.bool,
  location: PropTypes.object,
  changeTheme: PropTypes.func,
  navOpenKeys: PropTypes.array,
  changeOpenKeys: PropTypes.func,
};

export default Sider;
