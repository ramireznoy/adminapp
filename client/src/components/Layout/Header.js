import React from 'react';
import PropTypes from 'prop-types';
import { Menu, Icon, Popover } from 'antd';
import './Header.css';
import Menus from './Menu';
import { connect } from 'dva';

const SubMenu = Menu.SubMenu;

class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  handleClickMenu = (e) => {
    if (e.key === 'logout') {
      this.props.dispatch({ type: 'app/logout' });
    }
  }

  render() {
    return (
      <div className="header">
        {this.props.isNavbar
          ? <Popover placement="bottomLeft" onVisibleChange={this.props.switchMenuPopover} visible={this.props.menuPopoverVisible} overlayClassName="popovermenu" trigger="click" content={<Menus {...this.props} />}>
            <div className="button">
              <Icon type="bars" />
            </div>
          </Popover>
          : <div className="button" onClick={this.props.switchSider}>
            <Icon type={this.props.siderFold ? 'menu-unfold' : 'menu-fold'} />
          </div>}
        <div className="rightWarpper">
          <div className="button">
            <Icon type="mail" />
          </div>
          <Menu mode="horizontal" onClick={this.handleClickMenu}>
            <SubMenu style={{ float: 'right' }} title={<span> <Icon type="user" />
              {this.props.user.firstName} {this.props.user.lastName} </span>}
            >
              <Menu.Item key="logout">
                Sign out
              </Menu.Item>
            </SubMenu>
          </Menu>
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  menu: PropTypes.array,
  user: PropTypes.object,
  logout: PropTypes.func,
  switchSider: PropTypes.func,
  siderFold: PropTypes.bool,
  isNavbar: PropTypes.bool,
  menuPopoverVisible: PropTypes.bool,
  location: PropTypes.object,
  switchMenuPopover: PropTypes.func,
  navOpenKeys: PropTypes.array,
  changeOpenKeys: PropTypes.func,
};

export default connect()(Header);
