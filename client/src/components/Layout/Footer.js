import React from 'react';
import './Footer.css';
import { config } from '../../utils';

class Footer extends React.Component {
  render() {
    return (
      <div className="footer">
        {config.footerText}
      </div>
    );
  }
}

export default Footer;
