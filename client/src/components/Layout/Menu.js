import React from 'react';
import PropTypes from 'prop-types';
import { Menu, Icon } from 'antd';
import { Link } from 'dva/router';
import pathToRegexp from 'path-to-regexp';
import { arrayToTree, queryArray } from '../../utils';

class Menus extends React.Component {
  constructor(props) {
    super(props);

    // Generate tree
    this.menuTree = arrayToTree(props.menu.filter(_ => _.mpid !== -1), 'id', 'mpid');
    this.levelMap = {};
  }

  // Generate menu recursively
  getMenus = (menuTreeN, siderFoldN) => {
    return menuTreeN.map(item => {
      if (item.children) {
        if (item.mpid) {
          this.levelMap[item.id] = item.mpid;
        }
        return (
          <Menu.SubMenu
            key={item.id}
            title={<span>
              {item.icon && <Icon type={item.icon} />}
              {(!siderFoldN || this.menuTree.indexOf(item) < 0) && item.name}
            </span>}
          >
            {this.getMenus(item.children, siderFoldN)}
          </Menu.SubMenu>
        );
      }
      return (
        <Menu.Item key={item.id}>
          <Link to={item.router}>
            {item.icon && <Icon type={item.icon} />}
            {(!siderFoldN || this.menuTree.indexOf(item) < 0) && item.name}
          </Link>
        </Menu.Item>
      );
    });
  };

  // Keep selected
  getAncestorKeys = (key) => {
    let map = {};
    const getParent = (index) => {
      const result = [String(this.levelMap[index])];
      if (this.levelMap[result[0]]) {
        result.unshift(getParent(result[0])[0]);
      }
      return result;
    };
    for (let index in this.levelMap) {
      if ({}.hasOwnProperty.call(this.levelMap, index)) {
        map[index] = getParent(index);
      }
    }
    return map[key] || [];
  };

  onOpenChange = (openKeys) => {
    const latestOpenKey = openKeys.find(key => !(this.props.navOpenKeys.indexOf(key) > -1));
    const latestCloseKey = this.props.navOpenKeys.find(key => !(openKeys.indexOf(key) > -1));
    let nextOpenKeys = [];
    if (latestOpenKey) {
      nextOpenKeys = this.getAncestorKeys(latestOpenKey).concat(latestOpenKey);
    }
    if (latestCloseKey) {
      nextOpenKeys = this.getAncestorKeys(latestCloseKey);
    }
    this.props.changeOpenKeys(nextOpenKeys);
  };

  render() {
    const menuItems = this.getMenus(this.menuTree, this.props.siderFold);
    const menuProps = !this.props.siderFold ? {
      onOpenChange: this.onOpenChange,
      openKeys: this.props.navOpenKeys,
    } : {};
    // Look for the selected route
    let currentMenu;
    let defaultSelectedKeys;
    for (let item of this.props.menu) {
      if (item.router && pathToRegexp(item.router).exec(this.props.location.pathname)) {
        currentMenu = item;
        break;
      }
    }
    const getPathArray = (array, current, pid, id) => {
      let result = [String(current[id])];
      const getPath = (item) => {
        if (item && item[pid]) {
          result.unshift(String(item[pid]));
          getPath(queryArray(array, item[pid], id));
        }
      };
      getPath(current);
      return result;
    };
    if (currentMenu) {
      defaultSelectedKeys = getPathArray(this.props.menu, currentMenu, 'mpid', 'id');
    }
    return (
      <Menu
        {...menuProps}
        mode={this.props.siderFold ? 'vertical' : 'inline'}
        theme={this.props.darkTheme ? 'dark' : 'light'}
        onClick={this.props.handleClickNavMenu}
        defaultSelectedKeys={defaultSelectedKeys}
      >
        {menuItems}
      </Menu>
    );
  }
}

Menus.propTypes = {
  menu: PropTypes.array,
  siderFold: PropTypes.bool,
  darkTheme: PropTypes.bool,
  location: PropTypes.object,
  handleClickNavMenu: PropTypes.func,
  navOpenKeys: PropTypes.array,
  changeOpenKeys: PropTypes.func,
};

export default Menus;
