import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Button, Col, Card, Row, Icon, Tag, message } from 'antd';

const spaced = {
  margin: '10px'
};

const usePointer = {
  cursor: 'pointer',
  border: '1px solid #ddd',
  marginBottom: '10px'
};

const statusMap = {
  'PENDING': { title: 'Pending', color: 'gray'},
  'VERIFIED': { title: 'Verified', color: 'green'},
  'NON_VERIFIED': { title: 'Non Verified', color: 'orange'},
  'REJECTED': { title: 'Rejected', color: 'red'},
};

class DocumentView extends React.Component {
  constructor(props) {
    super(props);
  }

  onReject = (uploadId, e) => {
    this.props.dispatch({ type: 'customers/rejectDocument', payload: { uploadId: uploadId } });
  }

  onProcess = (uploadId, e) => {
    message.info('This feature will be available soon: Document Processing request for ' + uploadId);
    //this.props.dispatch({ type: 'customers/processDocument', payload: { uploadId: uploadId } });
  }

  onAccept = (uploadId, e) => {
    this.props.dispatch({ type: 'customers/setDocumentAsVerified', payload: { uploadId: uploadId } });
  }

  getStatusTag = (status) => {
    return <Tag color={statusMap[status].color}>{statusMap[status].title}</Tag>;
  }

  getFullDocument = (id, e) => {
    if (id) {
      this.props.dispatch({ type: 'customers/showFullDocument', documentId: id });
    }
  }

  render() {
    let image = 'data:' + this.props.document.mimeType + ';base64,' + this.props.document.fileData;
    const size = this.props.customers.documentsModalMode === 'list' ? 12 : 24;
    const processButton = this.props.document.status === 'PENDING' ? <Button type="info" onClick={e => this.onProcess(this.props.document._id, e)}><Icon type="cloud-upload" />Process</Button> : '';
    const acceptButton = this.props.document.status !== 'VERIFIED' ? <Button type="primary" onClick={e => this.onAccept(this.props.document._id, e)}>Accept<Icon type="check" /></Button> : '';
    return (
      <Col span={size}>
        <Card
          title={this.props.document.fileName}
          extra={this.getStatusTag(this.props.document.status)}
          style={spaced}
        >
          <img width="100%" alt="example" src={image} onClick={e => this.getFullDocument(this.props.document._id, e)} style={usePointer}/>
          <Row type="flex" justify="center">
            <Button.Group size="large">
              <Button type="default" onClick={e => this.onReject(this.props.document._id, e)}><Icon type="delete" />Reject</Button>
              {processButton}
              {acceptButton}
            </Button.Group>
          </Row>
        </Card>
      </Col>
    );
  }
}

DocumentView.propTypes = {
  document: PropTypes.object,
};

// Mapping the properties to connect the component to the store
const mapStateToProps = (state) => {
  return {
    customers: state.customers,
  };
};

export default connect(mapStateToProps)(DocumentView);
