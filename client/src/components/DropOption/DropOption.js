import React from 'react';
import PropTypes from 'prop-types';
import { Dropdown, Button, Icon, Menu } from 'antd';

class DropOption extends React.Component {
  constructor(props) {
    super(props);
    this.menu = this.props.menuOptions.map(item =>
      <Menu.Item key={item.key}><Icon type={item.icon} /> {item.name}</Menu.Item>
    );
  }
  render() {
    return (<Dropdown
      overlay={<Menu onClick={this.props.onMenuClick}>{this.menu}</Menu>}
      {...this.props.dropdownProps}
    >
      <Button style={{ border: 'none', ...this.props.buttonStyle }}>
        <Icon style={{ marginRight: 2 }} type="bars" />
        <Icon type="down" />
      </Button>
    </Dropdown>);
  }
}

DropOption.propTypes = {
  onMenuClick: PropTypes.func,
  menuOptions: PropTypes.array.isRequired,
  buttonStyle: PropTypes.object,
  dropdownProps: PropTypes.object,
};

export default DropOption;
