import { feathers } from '../utils';

export async function logout(params) {
  return feathers.logout()
    .catch(error => Object.assign(new Error('Logout Error'), error));
}

// Use authorization header to get User details
export async function userDetail(params) {
  return feathers.service('users').get(params.userId)
    .catch(error => Object.assign(new Error('Get User Error'), error));
}
