import { config, feathers } from '../utils';

const { api } = config;
const { userLogin, userEmail, userVerify, userReset } = api;

export async function login(data) {
  return feathers.authenticate({
    strategy: 'other',
    ...data
  }).catch((error) => {
    return Object.assign(new Error('Login Error'), error);
  });
}

export async function emailTo(data) {
  // return request({
  //   url: userEmail,
  //   method: 'post',
  //   data,
  // });
}

export async function verify(token) {
  // return request({
  //   url: userVerify + '/'.concat(token),
  //   method: 'get',
  // });
}

export async function resetPassword(data) {
  // return request({
  //   url: userReset,
  //   method: 'post',
  //   data,
  // });
}
