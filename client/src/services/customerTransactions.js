import { feathers } from '../utils';
const requestsService = feathers.service('requests');
const usersService = feathers.service('users');
const transactionsService = feathers.service('internal-transaction');
const accountsService = feathers.service('currency-accounts');

const getCustomerInfo = function includeCustomerInfo(request) {
  return new Promise((resolve, reject) => {
    usersService.find({ query: { _id: request.owner } }).then((result) => {
      resolve({
        ...request,
        customer: result.data[0],
      });
    }).catch((error) => { reject(error); });
  });
};

const getTransactionInfo = function includeTransactionInfo(request) {
  return new Promise((resolve, reject) => {
    transactionsService.find({ query: { _id: request.payload.transactionId } }).then((result) => {
      resolve({
        ...request,
        transaction: result.data[0],
      });
    }).catch((error) => { reject(error); });
  });
};

const getAccountInfo = function includeAccountInfo(request) {
  return new Promise((resolve, reject) => {
    // TODO: High priority! This is a temporary patch to distinguish between transactions for BTC and Fiat currencies
    // This is not a maintanable situation. A fix for coherent transaction format and usage needs to be done.
    let patchedId = request.transaction.currencyAccountOrigin;
    if (request.transaction.currency !== 'BTC') {
      patchedId = request.transaction.currencyAccountDestination;
    }
    // End of patch
    accountsService.find({ query: { _id: patchedId } }).then((result) => {
      resolve({
        ...request,
        account: result.data[0],
      });
    }).catch((error) => { reject(error); });
  });
};

export async function query(params) {
  return requestsService.find({
    query: {
      $limit: 1000,
      request: { $in : params.requestTypes },
      stage: params.stage,
    }
  }).then((requests) => {
    return Promise.all(requests.data.map(getCustomerInfo));
  }).then((withUser) => {
    return Promise.all(withUser.map(getTransactionInfo));
  }).then((withTransactions) => {
    return Promise.all(withTransactions.map(getAccountInfo));
  }).catch((error) => {
    Object.assign(new Error('Listing Error'), error);
  });
}

export async function declineRequest(params) {
  return requestsService.get(params.requestId).then((request) => {
    return requestsService.update(request._id, {
      $set: {
        token: 'DATA',
        stage: 'COMPLETED',
        updatedAt: Date.now(),
        payload: {
          ...request.payload,
          result: 'FAIL',
          status: 'DECLINED',
          comment: 'Internal Audit Veredict',
          declineCode: 999
        }
      }
    });
  }).catch((error) => {
    Object.assign(new Error('Updating Error'), error);
  });
}

export async function approveRequest(params) {
  return requestsService.update(params.requestId, {
    $set: {
      token: 'DATA',
      stage: 'ORDERED',
      updatedAt: Date.now(),
    }
  }).catch((error) => {
    Object.assign(new Error('Updating Error'), error);
  });
}

export async function refundRequest(params) {
  return requestsService.update(params.requestId, {
    $set: {
      token: 'DATA',
      stage: 'REFUND_DEPOSIT',
      updatedAt: Date.now(),
    }
  }).catch((error) => {
    Object.assign(new Error('Refunding Error'), error);
  });
}
