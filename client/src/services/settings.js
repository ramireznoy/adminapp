import { feathers } from '../utils';
const settingsService = feathers.service('settings');

export async function query(payload) {
  return Promise.all(payload.servers.map(getServerSettings)).catch((error) => {
    return Object.assign(new Error('Get server settings error'), error);
  });
}

export async function getServerSettings(server) {
  return settingsService.find({
    query: {
      $server: server,
      $limit: 500,
    }
  }).then(result => {
    let config = {};
    const target = server.toLowerCase() + 'Server';
    config[target] = result.data;
    return config;
  }).catch((error) => {
    return { error: error};
  });
}

export async function updateServer(data) {
  return Promise.all(data.map(updateSetting)).catch((error) => {
    return Object.assign(new Error('Update server error'), error);
  });
}

export async function updateSetting(config) {
  return settingsService.update(config._id, {
    $server: config.server,
    $set: {
      value: config.value,
    },
  }).catch((error) => {
    return Object.assign(new Error('Update setting error'), error);
  });
}