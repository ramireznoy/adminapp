import { feathers } from '../utils';
const usersService = feathers.service('users');
const accountsService = feathers.service('currency-accounts');
const transactionsService = feathers.service('internal-transaction');
const uploadService = feathers.service('upload');

export async function query(params) {
  const emailRegexp = '.*' + params.email + '.*';
  const nameRegexp = '.*' + params.name + '.*';
  // console.log(emailRegexp);
  // console.log(new RegExp(emailRegexp));
  return usersService.find({
    query: {
      $limit: params.limit,
      $skip: params.skip,
      $and: [
        {
          $or: {
            emailAddress: new RegExp(emailRegexp),
            firstName: new RegExp(nameRegexp),
            lastName: new RegExp(nameRegexp),
            dates: params.createTime,
          }
        },
        { 'permissions.userview': true }
      ]
    }
  }).catch((error) => {
    return Object.assign(new Error('Listing Error'), error);
  });
}

export async function getCustomerAccounts(customer) {
  return accountsService.find({
    query: {
      _id: {
        $in: customer.currencyAccounts
      }
    }
  }).catch((error) => {
    return Object.assign(new Error('Account listing Error'), error);
  });
}

const getFileContent = function includeFileContent(request) {
  return new Promise((resolve, reject) => {
    uploadService.get(request._id).then((result) => {
      resolve({
        ...request,
        fileData: result.fileData,
      });
    }).catch((error) => { reject(error); });
  });
};

export async function getCustomerDocuments(customer) {
  return uploadService.find({
    query: {
      deleted: { $ne: true },
      owner: { $in: customer._id }
    }
  }).then((uploads) => {
    return Promise.all(uploads.data.map(getFileContent));
  }).catch((error) => {
    return Object.assign(new Error('Documents listing Error'), error);
  });
}

export async function getFullDocument(documentId) {
  return uploadService.find({
    query: {
      $or: [
        { _id: documentId },
        { fullFileRequest: 'full' },
      ]
    }
  }).catch((error) => {
    return Object.assign(new Error('Full document get error'), error);
  });
}

export async function setDocumentStatus(payload) {
  return uploadService.update(payload.id, {
    $set: {
      status: payload.status,
    }
  }).catch((error) => {
    return Object.assign(new Error('Upload update error'), error);
  });
}

export async function setCustomerVerification(payload) {
  return usersService.update(payload.id, {
    $set: {
      identityVerified: payload.identityVerified,
    }
  }).catch((error) => {
    return Object.assign(new Error('Custonmer update error'), error);
  });
}

export async function getAccountTransactions(account) {
  // IMPORTANT! This method asumes each user could have just one currency account per currency type
  return transactionsService.find({
    query: {
      currency: account.currencyType,
      owner: account.owner,
      $limit: 2000    // By the moment, get everything... Will require pagination eventually
    }
  }).catch((error) => {
    return Object.assign(new Error('Transactions listing Error'), error);
  });
}

export async function setCustomerDisabled(customerId, disabledState) {
  return usersService.update(customerId, {
    $set: {
      disabled: disabledState
    }
  }).catch((error) => {
    return Object.assign(new Error('Disabling customer Error'), error);
  });
}

export async function updateCustomer(customerId, data) {
  return usersService.update(customerId, {
    $set: {
      username: data.username,
      middleName: data.middleName,
      lastName: data.lastName,
      emailAddress: data.emailAddress,
      phone: {
        number: data.phone,
        verified: false
      },
      customerPrivilegeLevel: data.customerPrivilegeLevel,
      permissions: data.permissions
    }
  }).catch((error) => {
    return Object.assign(new Error('Disabling customer Error'), error);
  });
}

/*const request = function transactionRequest(account) {
return new Promise((resolve, reject) => {
  transactionsService.find({ query: { _id: { $in: account.txids }, $limit: 1000 } }).then((result) => {
    resolve(result.data);
  }).catch((error) => { reject(error); });
});
};

/*export async function getTransactions(accounts) {
  const actions = accounts.map(request);
  return Promise.all(actions);
}*/