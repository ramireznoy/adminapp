import dva from 'dva';
import 'babel-polyfill';
import createLoading from 'dva-loading';
import { browserHistory } from 'dva/router';
import { message } from 'antd';
import appModel from './models/app';
import appRouter from './router';

// 1. Initialize
const app = dva({
  ...createLoading({
    effects: true,
  }),
  history: browserHistory,
  onError(error) {
    message.error(error.message);
  },
});

// 2. Model
app.model(appModel);

// 3. Router
app.router(appRouter);

// 4. Start
app.start('#root');
