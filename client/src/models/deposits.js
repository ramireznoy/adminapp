import { query, declineRequest, approveRequest, refundRequest } from '../services/customerTransactions';
import { config, feathers } from '../utils';

const requestsService = feathers.service('requests');

export default {
  namespace: 'deposits',

  state: {
    list: []
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/deposits') {
          dispatch({
            type: 'query',
            payload: location.query,
          });
        }
      });
    },

    requestsWatcher({ dispatch }) {
      requestsService.on('created', (request) => {
        // Update list with the related data from the new Request object
        //console.log(request);
      });
    },
  },

  effects: {
    *query({ payload = {} }, { call, put }) {
      // Do not contemplate pagination by the moment...
      payload = {
        ...payload,
        requestTypes: ['CREATE_DEPOSIT_ORDER'],
        stage: 'COMPLETED',
      };
      const requests = yield call(query, payload);
      if (requests) {
        yield put({
          type: 'querySuccess',
          payload: requests,
        });
      }
    },
    *lock({ payload = {} }, { call, put }) {
      // Do something to lock the fund amount
    },
    *refund({ payload = {} }, { call, put }) {
      const request = yield call(refundRequest, payload);
      if (request) {
        yield put({
          type: 'removeRequest',
          payload: request._id,
        });
      }
    },
  },

  reducers: {
    querySuccess(state, { payload: requests }) {
      // Filtering just successful transactions
      const copy = requests.filter(e => e['transaction'].status === 'SUCCESS');
      return {
        ...state,
        list: copy,
      };
    },
    removeRequest(state, { payload: id }) {
      const copy = state.list.filter(e => e['_id'] !== id);
      return {
        ...state,
        list: copy,
      };
    },
  },
};
