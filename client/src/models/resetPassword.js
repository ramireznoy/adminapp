import {select} from 'redux-saga/effects';
import {getToken} from './selectors';

import { verify, resetPassword } from '../services/login';

export default {
  namespace: 'resetPassword',

  state: {
    verifying: true,
    saySorry: false,
    waiting: false,
    showError: false,
    showConfirmation: false,
    message: '',
    token: null,
  },
  
  effects: {
    *verify({
      payload,
    }, { put, call }) {
      const data = yield call(verify, payload.token);
      if (data.statusCode === 200) {
        if (data.success) {
          yield put({ type: 'tokenVerified', payload: { firstName: data.userFirstName, token: payload.token } });
        } else {
          yield put({ type: 'saySorry', payload: { message: 'you are not authorized. Plase, note that the links provided are valid for just 48h, then discarded after used.' } });
        }
      } else {
        yield put({ type: 'saySorry', payload: { message: 'There was an error trying to retrieve your authorization. Plase, try again later' } });
      }
    },
    *reset({
      payload,
    }, { put, call }) {
      yield put({ type: 'showWaiting' });
      const verificationToken = yield select(getToken);
      const finalPayload = {
        ...payload,
        token: verificationToken,
      };
      const data = yield call(resetPassword, finalPayload);
      yield put({ type: 'hideWaiting' });
      if (data.statusCode === 200) {
        if (data.success) {
          yield put({ type: 'resetDone' });
        } else {
          yield put({ type: 'showError' });
        }
      } else {
        yield put({ type: 'showError' });
      }
    },
  },

  reducers: {
    showWaiting(state) {
      return {
        ...state,
        wating: true,
      };
    },
    hideWaiting(state) {
      return {
        ...state,
        wating: false,
      };
    },
    showError(state) {
      return {
        ...state,
        showError: true,
      };
    },
    tokenVerified(state, { payload }) {
      return {
        ...state,
        saySorry: false,
        verifying: false,
        waiting: false,
        showError: false,
        showConfirmation: false,
        message: '',
        userFirstName: payload.firstName,
        token: payload.token,
      };
    },
    saySorry(state, { payload }) {
      return {
        ...state,
        saySorry: true,
        verifying: false,
        waiting: false,
        showError: false,
        showConfirmation: false,
        message: payload.message,
        token: null,
      };
    },
    resetDone(state) {
      return {
        ...state,
        verifying: false,
        saySorry: false,
        waiting: false,
        showError: false,
        showConfirmation: true,
        message: '',
        userFirstName: '',
        token: null,
      };
    }
  },
};
