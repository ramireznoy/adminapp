import { routerRedux } from 'dva/router';
import { login } from '../services/login';
import { queryURL } from '../utils';
import { config } from '../utils';
const { prefix } = config;

export default {
  namespace: 'login',

  state: {
    waiting: false,
    loginError: false,
    badCredentials: false,
  },

  effects: {
    *login({ payload }, { put, call }) {
      yield put({ type: 'showWaiting' });
      const data = yield call(login, payload);
      yield put({ type: 'hideWaiting' });
      if (data instanceof Error || data === undefined) {
        yield put({ type: 'badCredentials' });
      }
      else {
        const from = queryURL('from');
        yield put({ type: 'app/query' });
        if (from) {
          // TODO: solve the from problem with the roles permission... Or just remove the from feature
          // Right now, the quick fix is to just ignore the from parameter 
        }
        yield put(routerRedux.push('/'));
      }
    },
  },

  reducers: {
    showWaiting(state) {
      return {
        ...state,
        waiting: true,
        loginError: false,
        badCredentials: false,
      };
    },
    hideWaiting(state) {
      return {
        ...state,
        waiting: false,
      };
    },
    badCredentials(state) {
      return {
        ...state,
        badCredentials: true,
        loginError: true,
      };
    },
    resetErrors(state) {
      return {
        ...state,
        loginError: false,
      };
    }
  },
};
