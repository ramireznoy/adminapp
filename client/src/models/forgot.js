import { routerRedux } from 'dva/router';
import { emailTo } from '../services/login';
import { queryURL } from '../utils';

export default {
  namespace: 'forgot',

  state: {
    waiting: false,
    showError: false,
    showConfirmation: false,
  },

  effects: {
    *sendEmail({
      payload,
    }, { put, call }) {
      yield put({ type: 'showWaiting' });
      const data = yield call(emailTo, payload);
      yield put({ type: 'hideWaiting' });
      if (data.statusCode === 200) {
        if (data.success) {
          yield put({ type: 'emailSent' });
        } else {
          yield put({ type: 'showError' });
        }
      } else {
        yield put({ type: 'showError' });
      }
    },
  },

  reducers: {
    showWaiting(state) {
      return {
        ...state,
        waiting: true,
      };
    },
    hideWaiting(state) {
      return {
        ...state,
        waiting: false,
      };
    },
    showError(state) {
      return {
        ...state,
        showError: true,
      };
    },
    emailSent(state) {
      return {
        ...state,
        showConfirmation: true,
      };
    },
    reset(state) {
      return {
        ...state,
        waiting: false,
        showError: false,
        showConfirmation: false,
      };
    },
  },
};
