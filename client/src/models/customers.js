import modelExtend from 'dva-model-extend';
import { select } from 'redux-saga/effects';
import {
  query,
  getCustomerAccounts,
  getAccountTransactions,
  setCustomerDisabled,
  updateCustomer,
  getCustomerDocuments,
  getFullDocument,
  setDocumentStatus,
  setCustomerVerification
} from '../services/customers';
import { pageModel } from './common';
import { getCustomersPagination, getUploadOwner, isFullyVerified } from './selectors';

export default modelExtend(pageModel, {
  namespace: 'customers',

  state: {
    accountModalVisible: false,
    customerModalVisible: false,
    documentsModalVisible: false,
    hasNoAccountsAlert: false,
    loadingDocuments: true,
    accountList: [],
    currentCustomer: {},
    documentList: [],
    documentsModalMode: 'list',
    currentDocument: {},
    spinText: 'Loading documents list, please wait...',
    activeDocumentGroup: '',
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/customers') {
          dispatch({
            type: 'query',
            payload: location.query,
          });
        }
      });
    },
  },

  effects: {
    *query({ payload = {} }, { call, put }) {
      const pagination = yield select(getCustomersPagination);
      payload = {
        ...payload,
        limit: pagination.limit,
        skip: pagination.limit * (pagination.current - 1),
      };
      const data = yield call(query, payload);
      if (data && data.total) {
        yield put({
          type: 'querySuccess',
          payload: data,
        });
      }
    },

    *disable(payload = {}, { call, put }) {
      const customerId = payload.customerId;
      const disabledState = payload.disabledState;
      if (customerId) {
        const results = yield call(setCustomerDisabled, customerId, disabledState);
        if (results) {
          yield put({ type: 'setCustomerDisabled', payload: { customerId, disabledState } });
        }
      }
    },

    *updateCustomer({ payload = {} }, { call, put }) {
      const customerId = payload.customer;
      const data = payload.values;
      if (customerId) {
        const result = yield call(updateCustomer, customerId, data);
        if (result) {
          yield put({ type: 'updateCustomer', payload: { customerId, result } });
        }
      }
    },

    *getAccounts({ payload = {} }, { call, put }) {
      if (!payload.customer.currencyAccounts || payload.customer.currencyAccounts.length === 0) {
        yield put({ type: 'showAccountsAlert' });
      } else {
        const accounts = yield call(getCustomerAccounts, payload.customer);
        if (accounts) {
          accounts.data.forEach(element => {
            element.transactions = [];
          });
          yield put({ type: 'showAccounts', payload: accounts.data });
        }
      }
    },

    *getDocuments({ payload = {} }, { call, put }) {
      yield put({ type: 'showDocumentsModal' });
      const documents = yield call(getCustomerDocuments, payload.customer);
      if (documents) {
        yield put({ type: 'loadDocumentList', payload: { documents } });
      }
    },

    *getTransactions({ account = {} }, { call, put }) {
      if (account.txids.length > 0) {
        const results = yield call(getAccountTransactions, account);
        if (results) {
          yield put({ type: 'updateAccount', payload: { accountId: account._id, data: results.data } });
        }
      }
    },

    *showFullDocument({ documentId = {} }, { call, put }) {
      yield put({ type: 'setDocumentLoadingMode', payload: { loading: true } });
      const fullDocument = yield call(getFullDocument, documentId);
      if (fullDocument) {
        yield put({ type: 'loadFullDocument', document: fullDocument.data[0] });
      }
    },

    *checkCustomerIdentityVerification({ payload = {} }, { call, put }) {
      const owner = yield select(getUploadOwner, payload.id);
      const isVerified = yield select(isFullyVerified, owner);
      const result = yield call(setCustomerVerification, { id: owner, identityVerified: isVerified });
      if (result._id) {
        yield put({ type: 'updateCustomerIdentityVerification', payload: { customer: result } });
      }
    },

    *setDocumentAsVerified({ payload = {} }, { call, put }) {
      const result = yield call(setDocumentStatus, { id: payload.uploadId, status: 'VERIFIED' });
      if (result.status === 'VERIFIED') {
        yield put({ type: 'updateDocumentStatus', payload: { id: payload.uploadId, status: 'VERIFIED' } });
        yield put({ type: 'checkCustomerIdentityVerification', payload: { id: payload.uploadId } });
      }
    },

    *rejectDocument({ payload = {} }, { call, put }) {
      const result = yield call(setDocumentStatus, { id: payload.uploadId, status: 'REJECTED' });
      if (result.status === 'REJECTED') {
        yield put({ type: 'updateDocumentStatus', payload: { id: payload.uploadId, status: 'REJECTED' } });
        yield put({ type: 'checkCustomerIdentityVerification', payload: { id: payload.uploadId } });
      }
    },

  },

  reducers: {

    showAccounts(state, { payload }) {
      return {
        ...state,
        accountList: payload,
        accountsModalVisible: true
      };
    },

    showCustomer(state, { payload }) {
      return {
        ...state,
        currentCustomer: payload.customer,
        customerModalVisible: true,
      };
    },

    showDocumentsModal(state) {
      return {
        ...state,
        documentsModalVisible: true,
        loadingDocuments: true,
        spinText: 'Loading documents list, please wait...',
      };
    },

    loadDocumentList(state, { payload }) {
      let groupedDocuments = [];
      for (var i = 0; i < payload.documents.length; i++) {
        if (!groupedDocuments[payload.documents[i].group]) {
          groupedDocuments[payload.documents[i].group] = [];
        }
        groupedDocuments[payload.documents[i].group].push(payload.documents[i]);
      }
      let activeTab = '';
      if (Object.keys(groupedDocuments).length > 0) {
        activeTab = Object.keys(groupedDocuments)[0];
      }
      return {
        ...state,
        documentList: groupedDocuments,
        loadingDocuments: false,
        documentsModalMode: 'list',
        activeDocumentGroup: activeTab,
      };
    },

    setDocumentLoadingMode(state, { payload }) {
      return {
        ...state,
        loadingDocuments: payload.loading,
        spinText: 'Loading full document, please wait...',
      };
    },

    loadFullDocument(state, fullDocument) {
      return {
        ...state,
        currentDocument: fullDocument.document,
        loadingDocuments: false,
        documentsModalMode: 'single',
      };
    },

    switchToDocumentsList(state) {
      return {
        ...state,
        documentsModalMode: 'list',
      };
    },

    setCustomerDisabled(state, { payload }) {
      let newCustomersList = [];
      state.list.forEach(element => {
        if (element._id === payload.customerId) {
          element.disabled = payload.disabledState;
        }
        newCustomersList.push(element);
      });
      return {
        ...state,
        list: newCustomersList,
      };
    },

    updateDocumentStatus(state, { payload }) {
      let newList = [];
      let documents = [];
      for (let [key, value] of Object.entries(state.documentList)) {
        documents = [];
        for (var i = 0; i < value.length; i++) {
          documents.push(state.documentList[key][i]);
          if (documents[i]._id === payload.id) {
            documents[i].status = payload.status;
          }
        }
        newList[key] = documents;
      }
      return {
        ...state,
        documentList: newList,
        currentDocument: {
          ...state.currentDocument,
          status: payload.id === state.currentDocument._id ? payload.status : state.currentDocument.status,
        }
      };
    },

    updateCustomer(state, { payload }) {
      let newCustomersList = [];
      state.list.forEach(element => {
        if (element._id === payload.customerId) {
          element.username = payload.result.username;
          element.firstName = payload.result.firstName;
          element.middleName = payload.result.middleName;
          element.lastName = payload.result.lastName;
          element.emailAddress = payload.result.emailAddress;
          element.phone = {
            ...element.phone,
            number: payload.result.phone.number,
          };
          element.customerPrivilegeLevel = payload.result.customerPrivilegeLevel;
          element.permissions = payload.result.permissions;
        }
        newCustomersList.push(element);
      });
      return {
        ...state,
        list: newCustomersList,
        customerModalVisible: false,
      };
    },

    updateCustomerIdentityVerification(state, { payload }) {
      let newCustomersList = [];
      state.list.forEach(element => {
        if (element._id === payload.customer._id) {
          element = payload.customer;
        }
        newCustomersList.push(element);
      });
      return {
        ...state,
        list: newCustomersList,
      };
    },

    hideAccounts(state) {
      return {
        ...state,
        accountsModalVisible: false
      };
    },

    hideCustomer(state) {
      return {
        ...state,
        customerModalVisible: false
      };
    },

    hideDocuments(state) {
      return {
        ...state,
        documentsModalVisible: false,
        loadingDocuments: true,
      };
    },

    updateAccount(state, { payload }) {
      let newAccountList = [];
      state.accountList.forEach(element => {
        if (element._id === payload.accountId) {
          element.transactions = payload.data;
        }
        newAccountList.push(element);
      });
      return {
        ...state,
        accountList: newAccountList,
      };
    },

    showAccountsAlert(state) {
      return { ...state, hasNoAccountsAlert: true };
    },

    hideAccountsAlert(state) {
      return { ...state, hasNoAccountsAlert: false };
    },

    paginationEvent(state, { payload }) {
      return {
        ...state,
        pagination: {
          ...state.pagination,
          current: payload.current,
          limit: payload.limit,
        }
      };
    }

  },
});

/*
    usersWatcher({ dispatch }) {
      usersService.on('created', (user) => {
        // Update table and/or paginator total
        console.log(user);
      });
      usersService.on('patched', (user) => {
        // Update table if the user is in the curent set
        console.log(user);
      });
      usersService.on('removed', (user) => {
        // Update table and/or paginator total
        console.log(user);
      });
    },
  },
});*/
