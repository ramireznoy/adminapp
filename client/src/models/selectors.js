export const getToken = (store) => {
  return store.resetPassword.token;
};

export const getUser = (store) => {
  return store.user;
};

export const getCustomersPagination = (store) => {
  return store.customers.pagination;
};

export const getUploadOwner = (store, uploadId) => {
  for (let [key, value] of Object.entries(store.customers.documentList)) {
    for (var i = 0; i < value.length; i++) {
      if (store.customers.documentList[key][i]._id === uploadId) {
        return store.customers.documentList[key][i].owner;
      }
    }
  }
};

export const isFullyVerified = (store, owner) => {
  let verified = false;
  let probe = true;
  if (store.customers.documentList['id'].length > 0) {
    for (var i = 0; i < store.customers.documentList['id'].length; i++) {
      if (store.customers.documentList['id'][i].owner === owner) {
        probe = probe && store.customers.documentList['id'][i].status === 'VERIFIED';
      }
    }
  }
  return verified || probe;
};

export const getServerSettings = (store, server) => {
  for (let [key, value] of Object.entries(store.settings)) {
    if (value.serverName === server) {
      return value.settings;
    }
  }
};
