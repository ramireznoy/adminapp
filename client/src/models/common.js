import modelExtend from 'dva-model-extend';

const model = {
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};

const pageModel = modelExtend(model, {

  state: {
    list: [],
    pagination: {
      showSizeChanger: true,
      showQuickJumper: true,
      showTotal: total => `Total entries: ${total}`,
      current: 1,
      total: 0,
      limit: 10,
    },
  },

  reducers: {
    querySuccess(state, { payload }) {
      const { data } = payload;
      return {
        ...state,
        list: data,
        pagination: {
          ...state.pagination,
          total: payload.total,
          limit: payload.limit,
        },
      };
    },
  },

});

export {
  model,
  pageModel,
};
