import { query, updateServer } from '../services/settings';
import { getServerSettings } from './selectors';
import { select } from 'redux-saga/effects';

export default {
  namespace: 'settings',

  state: {
    dataServer: {
      serverName: 'DATA',
      icon: 'global',
      updating: false,
      settings: {}
    },
    fiatServer: {
      serverName: 'FIAT',
      icon: 'credit-card',
      updating: false,
      settings: {}
    },
    btcServer: {
      serverName: 'BTC',
      icon: 'wallet',
      updating: false,
      settings: {}
    },
    webServer: {
      serverName: 'WEB',
      icon: 'ie',
      updating: false,
      settings: {}
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/settings') {
          dispatch({
            type: 'query',
            payload: { servers: ['DATA', 'FIAT'] }
          });
        }
      });
    },
  },

  effects: {
    *query({ payload = {} }, { call, put }) {
      const result = yield call(query, payload);
      if (result && result.length > 0) {
        let allSettings = {};
        for (let [key, value] of Object.entries(result)) {
          if (!value.error) {
            allSettings = {
              ...allSettings,
              ...value
            };
          }
        }
        yield put({
          type: 'querySuccess',
          payload: allSettings,
        });
      }
    },

    *updateServer({ payload = {} }, { call, put }) {
      yield put({
        type: 'setUpdating',
        payload: {
          updating: true,
          server: payload.server,
        },
      });
      let settings = [];
      let currentSettings = yield select(getServerSettings, payload.server);
      for (let [key, value] of Object.entries(currentSettings)) {
        if (value.value !== payload.values[value.name]) {
          value.value = payload.values[value.name];
          value.server = payload.server;
          settings.push(value);
        }
      }
      if (settings.length > 0) {
        yield call(updateServer, settings);
      }
      yield put({
        type: 'setUpdating',
        payload: {
          updating: false,
          server: payload.server,
        },
      });
    },
  },

  reducers: {

    querySuccess(state, { payload }) {
      let newState = {};
      for (let [key, value] of Object.entries(payload)) {
        newState[key] = {
          ...state[key],
          settings: payload[key],
        };
      }
      return {
        ...state,
        ...newState
      };
    },

    setUpdating(state, { payload }) {
      const target = payload.server.toLowerCase() + 'Server';
      let newState = {};
      for (let [key, value] of Object.entries(state)) {
        if (key === target) {
          value['updating'] = payload.updating;
        }
        newState[key] = value;
      }
      return newState;
    },
  }

};