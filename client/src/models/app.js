import { routerRedux } from 'dva/router';
import { parse } from 'qs';
import { userDetail, logout } from '../services/app';
import { config, feathers } from '../utils';
import { getUser } from '../models/selectors';

//var jwtDecode = require('jwt-decode');
import jwt_decode from 'jwt-decode';

const { prefix } = config;

export default {
  namespace: 'app',
  state: {
    user: {},
    menuPopoverVisible: false,
    siderFold: localStorage.getItem(`${prefix}siderFold`) === 'true',
    darkTheme: true,// Will use the dark theme always. (localStorage.getItem(`${prefix}darkTheme`) === 'true')
    isNavbar: document.body.clientWidth < 769,
    navOpenKeys: JSON.parse(localStorage.getItem(`${prefix}navOpenKeys`)) || [],
  },
  subscriptions: {
    setup({ dispatch }) {
      dispatch({ type: 'query' });
      feathers.authenticate().then(accessToken => {
        dispatch({ type: 'app/query' });
      });
      let tid;
      window.onresize = () => {
        clearTimeout(tid);
        tid = setTimeout(() => {
          dispatch({ type: 'changeNavbar' });
        }, 300);
      };
    },

  },
  effects: {
    *query({
      payload,
    }, { call, put }) {
      const token = localStorage.getItem('feathers-jwt');
      let userId = null;
      if (token) {
        const decoded = jwt_decode(token);
        userId = decoded._id;
        payload = {
          ...payload,
          userId: userId,
          token: token,
        };
        const data = yield call(userDetail, parse(payload));
        if (data instanceof Error) {
          if (config.openPages && config.openPages.indexOf(window.location.pathname) < 0) {
            let from = window.location.pathname;
            window.window.location = `${window.location.origin}/login?from=${from}`;
          }
        }
        else {
          yield put({
            type: 'querySuccess',
            payload: data,
          });
          if (config.openPages && config.openPages.indexOf(window.location.pathname) > -1) {
            yield put(routerRedux.push('/'));
          }
        }
      } else {
        if (config.openPages && config.openPages.indexOf(window.location.pathname) < 0) {
          let from = window.location.pathname;
          window.window.location = `${window.location.origin}/login?from=${from}`;
        }
      }
    },

    *logout({
      payload,
    }, { call, put }) {
      const data = yield call(logout, {});
      if (!data) {
        yield put({ type: 'query' });
      } else {
        throw (data);
      }
    },

    *changeNavbar({
      payload,
    }, { put, select }) {
      const { app } = yield (select(_ => _));
      const isNavbar = document.body.clientWidth < 769;
      if (isNavbar !== app.isNavbar) {
        yield put({ type: 'handleNavbar', payload: isNavbar });
      }
    },

  },
  reducers: {
    querySuccess(state, { payload: user }) {
      return {
        ...state,
        user,
      };
    },

    switchSider(state) {
      localStorage.setItem(`${prefix}siderFold`, !state.siderFold);
      return {
        ...state,
        siderFold: !state.siderFold,
      };
    },

    switchTheme(state) {
      localStorage.setItem(`${prefix}darkTheme`, !state.darkTheme);
      return {
        ...state,
        darkTheme: !state.darkTheme,
      };
    },

    switchMenuPopver(state) {
      return {
        ...state,
        menuPopoverVisible: !state.menuPopoverVisible,
      };
    },

    handleNavbar(state, { payload }) {
      return {
        ...state,
        isNavbar: payload,
      };
    },

    handleNavOpenKeys(state, { payload: navOpenKeys }) {
      return {
        ...state,
        ...navOpenKeys,
      };
    },
  },
};
