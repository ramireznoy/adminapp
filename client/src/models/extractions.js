import { query, declineRequest, approveRequest } from '../services/customerTransactions';
import { config, feathers } from '../utils';

const requestsService = feathers.service('requests');

export default {
  namespace: 'extractions',

  state: {
    list: []
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/extractions') {
          dispatch({
            type: 'query',
            payload: location.query,
          });
        }
      });
    },

    requestsWatcher({ dispatch }) {
      requestsService.on('created', (request) => {
        // Update list with the related data from the new Request object
        // console.log(request);
      });
    },
  },


  effects: {
    *query({ payload = {} }, { call, put }) {
      // Do not contemplate pagination by the moment...
      payload = {
        ...payload,
        requestTypes: ['CREATE_PAYOUT_ORDER','REQUEST_SEND_BITCOINS'],
        stage: 'REQUESTED',
      };
      const requests = yield call(query, payload);
      if (requests) {
        yield put({
          type: 'querySuccess',
          payload: requests,
        });
      }
    },
    *decline({ payload = {} }, { call, put }) {
      const request = yield call(declineRequest, payload);
      if (request) {
        yield put({
          type: 'removeRequest',
          payload: request._id,
        });
      }
    },
    *approve({ payload = {} }, { call, put }) {
      const request = yield call(approveRequest, payload);
      if (request) {
        yield put({
          type: 'removeRequest',
          payload: request._id,
        });
      }
    },
  },

  reducers: {
    querySuccess(state, { payload: requests }) {
      return {
        ...state,
        list: requests,
      };
    },
    removeRequest(state, { payload: id }) {
      const copy = state.list.filter(e => e['_id'] !== id);
      return {
        ...state,
        list: copy,
      };
    },
  },
};
