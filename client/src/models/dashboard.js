import { parse } from 'qs';
import { query } from '../services/dashboard';
import { config } from '../utils';
const { prefix } = config;

export default {
  namespace: 'dashboard',

  state: {
    sales: [],
    totals: [],
    currencies: [],
  },

  subscriptions: {
    setup({ dispatch }) {
      dispatch({ type: 'query' });
    },
  },

  effects: {
    *query({
      payload,
    }, { call, put }) {
      const token = localStorage.getItem(`${prefix}token`);
      if (token) {
        payload = {
          ...payload,
          token: token,
        };
        const data = yield call(query, parse(payload));
        yield put({ type: 'updateState', payload: { ...data } });
      }
    },
  },

  reducers: {
    updateState(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};
