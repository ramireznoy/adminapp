import React from 'react';
import { Router } from 'dva/router';
import { LocaleProvider } from 'antd';
import PropTypes from 'prop-types';
import App from './routes/app';
import enUS from 'antd/lib/locale-provider/en_US';
import dashboardModel from './models/dashboard';
import dashboardRoute from './routes/dashboard';
import userModel from './models/user';
import userRoute from './routes/user';
import userDetailModel from './models/user/detail';
import userDetailRoute from './routes/user/detail';
import loginModel from './models/login';
import loginRoute from './routes/login';
import forgotModel from './models/forgot';
import forgotRoute from './routes/forgot';
import resetPasswordModel from './models/resetPassword';
import resetPasswordRoute from './routes/resetPassword';
import customersRoute from './routes/customers';
import customersModel from './models/customers';

import depositsRoute from './routes/deposits';
import depositsModel from './models/deposits';

import extractionsRoute from './routes/extractions';
import extractionsModel from './models/extractions';

import settingsRoute from './routes/settings';
import settingsModel from './models/settings';

import errorRoute from './routes/error';


const registerModel = (app, model) => {
  if (!(app._models.filter(m => m.namespace === model.namespace).length === 1)) {
    app.model(model);
  }
};

const Routers = function ({ history, app }) {
  const routes = [
    {
      path: '/',
      component: App,
      getIndexRoute(nextState, cb) {
        require.ensure([], (require) => {
          registerModel(app, dashboardModel);
          cb(null, { component: dashboardRoute });
        }, 'dashboard');
      },
      childRoutes: [
        {
          path: 'dashboard',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              registerModel(app, dashboardModel);
              cb(null, dashboardRoute);
            }, 'dashboard');
          },
        },
        {
          path: 'user',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              registerModel(app, userModel);
              cb(null, userRoute);
            }, 'user');
          },
        },
        {
          path: 'user/:id',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              registerModel(app, userDetailModel);
              cb(null, userDetailRoute);
            }, 'user-detail');
          },
        },
        {
          path: 'login',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              registerModel(app, loginModel);
              cb(null, loginRoute);
            }, 'login');
          },
        },
        {
          path: 'forgot',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              registerModel(app, forgotModel);
              cb(null, forgotRoute);
            }, 'forgot');
          },
        },
        {
          path: 'reset',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              registerModel(app, resetPasswordModel);
              cb(null, resetPasswordRoute);
            }, 'reset');
          },
        },
        {
          path: 'customers',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              registerModel(app, customersModel);
              cb(null, customersRoute);
            }, 'customers');
          },
        },
        {
          path: 'deposits',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              registerModel(app, depositsModel);
              cb(null, depositsRoute);
            }, 'deposits');
          },
        },
        {
          path: 'extractions',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              registerModel(app, extractionsModel);
              cb(null, extractionsRoute);
            }, 'extractions');
          },
        },
        {
          path: 'settings',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              registerModel(app, settingsModel);
              cb(null, settingsRoute);
            }, 'settings');
          },
        },
        {
          path: '*',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              cb(null, errorRoute);
            }, 'error');
          },
        },
      ],
    },
  ];

  return (
    <LocaleProvider locale={enUS}>
      <Router history={history} routes={routes} />
    </LocaleProvider>
  );
};

Routers.propTypes = {
  history: PropTypes.object,
  app: PropTypes.object,
};

export default Routers;
