// reset-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const reset = new Schema({
    userId: { type: Schema.ObjectId, required: true },
    userFirstName: { type: String, required: true },
    ttl: { type: Number, required: true }
  }, {
    timestamps: true
  });

  return mongooseClient.model('reset', reset);
};
