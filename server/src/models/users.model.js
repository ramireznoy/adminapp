// users-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const users = new Schema({
    name: String,
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    firstName: { type: String, required: true },
    middleName: { type: String },
    lastName: { type: String, required: true },
    emailAddress: { type: String, required: true, unique: true },
    userAvatar: { type: String },
    emailVerified: { type: Boolean, default: false },
    uuid: { type: String, required: true, unique: true },
    bitcoinWalletAddress: String,
    bitcoinWalletCount: { type: Number, required: true, default: 1 },
    bitcoinWallets: [{
      type: Schema.ObjectId,
      ref: 'wallets'
    }],
    currencyAccounts: [{
      type: Schema.ObjectId,
      ref: 'currencyAccounts'
    }],
    permissions: {
      userview: Boolean,
      canRead: Boolean,
      canEdit: Boolean,
      isAdmin: Boolean,
      canApi: Boolean
    },
    apiKey: { type: String, unique: true, sparse: true },
    meta: {
      age: Number,
      website: String
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    isVerified: { type: Boolean },
    verifyToken: { type: String },
    verifyExpires: { type: Date },
    verifyChanges: { type: Object },
    resetToken: { type: String },
    resetExpires: { type: Date },
    deleted: { type: Boolean },
    disabled: { type: Boolean }
  });

  return mongooseClient.model('users', users);
};
