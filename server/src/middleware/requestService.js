const crypt = require('../lib/crypt');
const config = require('config');

const SERVER_TITLE = config.get('serverTitle');

const ADMIN_SERVER = config.get('serverTitles.admin');
const BTC_SERVER = config.get('serverTitles.btc');
const DATA_SERVER = config.get('serverTitles.data');
const FIAT_SERVER = config.get('serverTitles.fiat');
const WEB_SERVER = config.get('serverTitles.web');


module.exports = function (app) {

  function init(app) {
    var requestService = app.service('requests');
    if (!requestService) {
      setTimeout(() => {
        init(app);
      }, 100);
    }
    else {
      function requestUpdated(request) {

        if (request.token === SERVER_TITLE && SERVER_TITLE === ADMIN_SERVER) {
          switch (request.request) {
            
            default:
              break;// case request.request
          }
        }
      }

      requestService.on('created', (request) => {
        requestUpdated(request);
      });
      requestService.on('updated', (request) => {
        requestUpdated(request);
      });
      requestService.on('patched', (request) => {
        requestUpdated(request);
      });

    }
  }
  init(app);
};