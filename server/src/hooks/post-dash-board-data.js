// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
//import { color } from '../utils/colors';

const Mock = require('mockjs');

const color = {
  green: '#64ea91',
  blue: '#8fc9fb',
  purple: '#d897eb',
  red: '#f69899',
  yellow: '#f8c82e',
  peach: '#f797d6',
  borderBase: '#e5e5e5',
  borderSplit: '#f4f4f4',
  grass: '#d6fbb5',
  sky: '#c1e0fc',
};

module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return function postDashBoardData(hook) {

    const Dashboard = Mock.mock({
      'sales|4': [
        {
          'name|+1': 2017,
          'Buys|200-500': 1,
          'Sales|300-550': 1,
        },
      ],
      currencies: [
        {
          name: 'BTC',
          percent: 43.3,
          status: 1,
        },
        {
          name: 'ETH',
          percent: 33.4,
          status: 2,
        },
        {
          name: 'LTC',
          percent: 34.6,
          status: 3,
        },
        {
          name: 'ADA',
          percent: 12.3,
          status: 4,
        },
        {
          name: 'TRX',
          percent: 3.3,
          status: 1,
        },
      ],
      totals: [
        {
          icon: 'credit-card',
          color: color.green,
          title: 'Sales',
          number: 2781,
        }, {
          icon: 'team',
          color: color.blue,
          title: 'New Customers',
          number: 3241,
        }, {
          icon: 'swap',
          color: color.purple,
          title: 'Transactions/hour',
          number: 253,
        }, {
          icon: 'exception',
          color: color.red,
          title: 'Refunds',
          number: 43,
        },
      ],
    });

    hook.result = Dashboard;
    return Promise.resolve(hook);
  };
};
