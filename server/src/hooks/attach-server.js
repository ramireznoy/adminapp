const config = require('config');
const featherClient = require('../lib/featherClient');


module.exports = function (options = {}) { // eslint-disable-line no-unused-vars

  return function attachDataServer(hook) {
    var payload = (hook.params && hook.params.payload) || hook.payload || {};
    var accessToken = payload.accessToken;

    hook.params = Object.assign(hook.params || {}, { dataServer: featherClient(options.serverHost || config.get(options.serverName), accessToken) }, {});
    return hook;

  };
};