// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

module.exports = function () {
  return function paymentAttempt(hook) {
    return new Promise((resolve, reject) => {
      if (hook.data.password !== hook.data.password2) {
        reject('Passwords are different');
      }
      if (!hook.data.password || hook.data.password === '') {
        reject('The password can not be empty');
      }
      
      hook.app.service('reset').get(hook.data.token).then((result) => {
        if (result) {
          hook.app.service('users').get(result.userId).then((result) => {
            reject('Hold On');
          });
        } else {
          reject('There are no user reset requests');
        }
      });
      //resolve(hook);
    });
  };
};