const { authenticate } = require('@feathersjs/authentication').hooks;
const { disallow } = require('feathers-hooks-common');
const search = require('feathers-mongodb-fuzzy-search');


module.exports = {
  before: {
    all: [
      //disallow('external')
      search({  // Does not seems to work... :-(
        fields: ['username', 'firstName', 'lastName', 'emailAddress']
      })
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
