/* eslint-disable no-unused-vars */
var dataServer = require('../../lib/feathersDataServerClient');
const search = require('feathers-mongodb-fuzzy-search');

var errors = require('@feathersjs/errors');

class Service {
  constructor(options) {
    this.options = options || {};
  }

  create(data, params) {
    return Promise.reject(new errors.BadRequest('Command cannot be performed'));
  }

  find(params) {
    return new Promise((resolve, reject) => {
      dataServer.service(params.query.service).find({
        query: {
          $limit: params.query.limit,
          $skip: params.query.skip,
        }
      }).then((result) => {
        resolve({
          success: true,
          result: result,
        });
      }).catch((error) => {
        reject({
          success: false,
          result: error.message,
        });
      });
    });
  }

  update(id, data, params) {
    // if (data.action === 'sendResetPwd') {
    //   return dataServer.service('authManagement').update(data);
    // }
  }
}


module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
