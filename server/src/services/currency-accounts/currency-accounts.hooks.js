const { authenticate } = require('@feathersjs/authentication').hooks;

const attachDataServer = require('../../hooks/attach-server')({ serverName: 'dataServer' });

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [attachDataServer],
    get: [attachDataServer],
    create: [attachDataServer],
    update: [attachDataServer],
    patch: [attachDataServer],
    remove: [attachDataServer]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
