class Service {
  constructor(options) {
    this.options = options || {};
    this.id = '_id';
  }

  find(params) {
    return params.dataServer.service('currency-accounts').find(params);
  }

  get(id, params) {
    return params.dataServer.service('currency-accounts').get(id); //, params);
  }

  create(data, params) {
    return params.dataServer.service('currency-accounts').create(data);//, params);
  }

  update(id, data, params) {
    return params.dataServer.service('currency-accounts').update(id, data);//, params);
  }

  patch(id, data, params) {
    return params.dataServer.service('currency-accounts').patch(id, data);//, params);
  }

  remove(id, params) {
    return params.dataServer.service('currency-accounts').remove(id);
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
