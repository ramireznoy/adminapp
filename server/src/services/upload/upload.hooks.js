const { authenticate } = require('@feathersjs/authentication').hooks;
const attachFiatServer = require('../../hooks/attach-server')({ serverName: 'fiatServer' });

module.exports = {
  before: {
    all: [ 
      authenticate('jwt'),
    ],
    find: [attachFiatServer],
    get: [attachFiatServer],
    create: [],
    update: [attachFiatServer],
    patch: [attachFiatServer],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};