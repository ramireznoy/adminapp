/* eslint-disable no-unused-vars */
class Service {
  constructor (options) {
    this.options = options || {};
    this.id = '_id';
  }

  find(params) {
    return params.dataServer.service('upload').find(params);
  }

  get(id, params) {
    return params.dataServer.service('upload').get(id); //, params);
  }

  create(data, params) {
    return params.dataServer.service('upload').create(data);//, params);
  }

  update(id, data, params) {
    return params.dataServer.service('upload').update(id, data);//, params);
  }

  patch(id, data, params) {
    return params.dataServer.service('upload').patch(id, data);//, params);
  }

  remove(id, params) {
    return params.dataServer.service('upload').remove(id);
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
