// Initializes the `messenger` service on path `/messenger`
const createService = require('feathers-mongoose');
const createModel = require('../../models/messenger.model');
const hooks = require('./messenger.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'messenger',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/messenger', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('messenger');

  service.hooks(hooks);
};
