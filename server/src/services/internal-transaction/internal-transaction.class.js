class Service {
  constructor(options) {
    this.options = options || {};
    this.id = '_id';
  }

  find(params) {
    return params.dataServer.service('internal-transaction').find(params);
  }

  get(id, params) {
    return params.dataServer.service('internal-transaction').get(id); //, params);
  }

  create(data, params) {
    return params.dataServer.service('internal-transaction').create(data);//, params);
  }

  update(id, data, params) {
    return params.dataServer.service('internal-transaction').update(id, data);//, params);
  }

  patch(id, data, params) {
    return params.dataServer.service('internal-transaction').patch(id, data);//, params);
  }

  remove(id, params) {
    return params.dataServer.service('internal-transaction').remove(id);
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
