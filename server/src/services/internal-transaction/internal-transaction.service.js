// Initializes the `internal-transaction` service on path `/internal-transaction`
const createService = require('./internal-transaction.class.js');
const hooks = require('./internal-transaction.hooks');

module.exports = function (app) {
  const paginate = app.get('paginate');

  const options = {
    name: 'internal-transaction',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/internal-transaction', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('internal-transaction');

  service.hooks(hooks);
};
