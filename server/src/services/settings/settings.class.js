/* eslint-disable no-unused-vars */
class Service {
  constructor(options) {
    this.options = options || {};
    this.id = '_id';
  }

  async find(params) {
    return params.dataServer.service('settings').find(params);
  }

  async get(id, params) {
    return params.dataServer.service('settings').get(id);
  }

  async create(data, params) {
    return params.dataServer.service('settings').create(data);
  }

  async update(id, data, params) {
    return params.dataServer.service('settings').update(id, data);
  }

  async patch(id, data, params) {
    return params.dataServer.service('settings').patch(id, data);
  }

  async remove(id, params) {
    return params.dataServer.service('settings').remove(id);
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
