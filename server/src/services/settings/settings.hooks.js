const { authenticate } = require('@feathersjs/authentication').hooks;
const Errors = require('@feathersjs/errors');
const attachServer = require('../../hooks/attach-server');
const errors = require('@feathersjs/errors');

module.exports = {
  before: {
    all: [authenticate('jwt'), attachHookServer],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};

function attachHookServer(hook) {
  let targetServer = 'data';
  if (hook.data && hook.data['$server']) {
    targetServer = hook.data['$server'];
    delete hook.data['$server'];
  }
  if (hook.params.query['$server']) {
    targetServer = hook.params.query['$server'];
    delete hook.params.query['$server'];
  }
  return attachServer({ serverName: targetServer.toLowerCase() + 'Server' })(hook);
}