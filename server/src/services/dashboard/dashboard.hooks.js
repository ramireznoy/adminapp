const { authenticate } = require('@feathersjs/authentication').hooks;
const postDashBoardData = require('../../hooks/post-dash-board-data');

module.exports = {
  before: {
    all: [/*authenticate('jwt')*/],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [postDashBoardData()],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
