// Initializes the `reset` service on path `/reset`
const createService = require('feathers-mongoose');
const createModel = require('../../models/reset.model');
const hooks = require('./reset.hooks');
const filters = require('./reset.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'reset',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/reset', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('reset');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
