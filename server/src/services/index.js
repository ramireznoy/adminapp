const users = require('./users/users.service.js');
const bitcoinTransactions = require('./bitcoin-transactions/bitcoin-transactions.service.js');
const wallets = require('./wallets/wallets.service.js');
const requests = require('./requests/requests.service.js');
const currencyAccounts = require('./currency-accounts/currency-accounts.service.js');
const paymentMethods = require('./payment-methods/payment-methods.service.js');
const enums = require('./enums/enums.service.js');
const tradeOffers = require('./trade-offers/trade-offers.service.js');

const commandProxy = require('./command-proxy/command-proxy.service.js');

const notifications = require('./notifications/notifications.service.js');

const tradingPairs = require('./trading-pairs/trading-pairs.service.js');

const internalTransaction = require('./internal-transaction/internal-transaction.service.js');

const messenger = require('./messenger/messenger.service.js');

const purchaseOrder = require('./purchase-order/purchase-order.service.js');

const dashboard = require('./dashboard/dashboard.service.js');

const reset = require('./reset/reset.service.js');

const upload = require('./upload/upload.service.js');

const settings = require('./settings/settings.service.js');

module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(bitcoinTransactions);
  app.configure(wallets);
  app.configure(requests);
  app.configure(currencyAccounts);
  app.configure(paymentMethods);
  app.configure(enums);
  app.configure(tradeOffers);
  app.configure(commandProxy);
  app.configure(notifications);
  app.configure(users);
  app.configure(tradingPairs);
  app.configure(internalTransaction);
  app.configure(messenger);
  app.configure(purchaseOrder);
  app.configure(dashboard);
  app.configure(reset);
  app.configure(upload);
  app.configure(settings);
};
