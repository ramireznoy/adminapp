const assert = require('assert');
const app = require('../../src/app');

describe('\'messenger\' service', () => {
  it('registered the service', () => {
    const service = app.service('messenger');

    assert.ok(service, 'Registered the service');
  });
});
