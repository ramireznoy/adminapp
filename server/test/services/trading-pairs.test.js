const assert = require('assert');
const app = require('../../src/app');

describe('\'trading-pairs\' service', () => {
  it('registered the service', () => {
    const service = app.service('trading-pairs');

    assert.ok(service, 'Registered the service');
  });
});
